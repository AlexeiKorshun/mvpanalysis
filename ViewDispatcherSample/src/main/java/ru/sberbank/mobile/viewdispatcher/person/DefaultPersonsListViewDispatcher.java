package ru.sberbank.mobile.viewdispatcher.person;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import java.util.List;

import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.common.person.ui.PersonViewHolder;
import ru.sberbank.mobile.common.person.ui.PersonsAdapter;
import ru.sberbank.mobile.core.view.BaseViewDispatcher;
import ru.sberbank.mobile.viewdispatcher.R;

/**
 * @author QuickNick.
 */

public class DefaultPersonsListViewDispatcher extends BaseViewDispatcher implements IPersonsListViewDispatcher,
        SwipeRefreshLayout.OnRefreshListener, PersonViewHolder.OnPersonClickListener {

    private final IPersonsListViewDispatcher.Listener mListener;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private PersonsAdapter mAdapter;

    public DefaultPersonsListViewDispatcher(Context context, FragmentManager fragmentManager, IPersonsListViewDispatcher.Listener listener) {
        super(context, fragmentManager);
        mListener = listener;
    }

    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recycler_view_activity, container, false);
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        mAdapter = new PersonsAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        return view;
    }

    @Override
    protected void onDestroyView() {
        mSwipeRefreshLayout = null;
        mRecyclerView = null;
        mProgressBar = null;
        mAdapter = null;
    }

    @Override
    public void setLoading(boolean loading, boolean firstLaunch) {
        mSwipeRefreshLayout.setRefreshing(loading);
        if (loading) {
            if (firstLaunch) {
                mSwipeRefreshLayout.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
            }
        } else {
            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }

    }

    @Override
    public void setPersons(List<Person> persons) {
        mAdapter.setPersons(persons);
    }

    @Override
    public Toolbar getToolbar() {
        return (Toolbar) mView.findViewById(R.id.toolbar);
    }

    // SwipeRefreshLayout.OnRefreshListener >>>

    @Override
    public void onRefresh() {
        mListener.onRefresh(this);
    }

    // <<< SwipeRefreshLayout.OnRefreshListener

    // PersonViewHolder.OnPersonClickListener >>>

    @Override
    public void onPersonClick(PersonViewHolder sender, Person person) {
        mListener.onPersonClick(this, person);
    }

    // <<< PersonViewHolder.OnPersonClickListener
}
