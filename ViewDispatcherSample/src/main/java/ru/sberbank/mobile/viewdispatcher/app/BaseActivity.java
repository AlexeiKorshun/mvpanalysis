package ru.sberbank.mobile.viewdispatcher.app;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import javax.inject.Inject;

import ru.sberbank.mobile.core.di.IHasComponent;
import ru.sberbank.mobile.core.observ.IContentWatcherCreator;
import ru.sberbank.mobile.core.observ.IPendingResultRetriever;
import ru.sberbank.mobile.core.observ.WatcherBundle;
import ru.sberbank.mobile.viewdispatcher.di.ViewDispatcherSampleAppComponent;

/**
 * @author QuickNick.
 */

public class BaseActivity extends AppCompatActivity {

    @Inject
    IPendingResultRetriever mPendingResultRetriever;

    private WatcherBundle mWatcherBundle;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDispatcherSampleAppComponent component = getComponent();
        component.inject(this);
        mWatcherBundle = new WatcherBundle(createContentWatcherCreator());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWatcherBundle.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWatcherBundle.onPause();
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mWatcherBundle.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mWatcherBundle.onSaveInstanceState(outState);
    }

    protected IPendingResultRetriever getPendingResultRetriever() {
        return mPendingResultRetriever;
    }

    protected WatcherBundle getWatcherBundle() {
        return mWatcherBundle;
    }

    protected <T> T getComponent() {
        IHasComponent<T> hasComponent = (IHasComponent<T>) getApplication();
        return hasComponent.getComponent();
    }

    protected IContentWatcherCreator createContentWatcherCreator() {
        return null;
    }
}
