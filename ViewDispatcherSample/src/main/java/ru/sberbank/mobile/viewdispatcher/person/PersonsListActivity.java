package ru.sberbank.mobile.viewdispatcher.person;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuItem;

import java.util.List;

import javax.inject.Inject;

import ru.sberbank.mobile.common.person.IPersonsManager;
import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.observ.IContentWatcher;
import ru.sberbank.mobile.core.observ.SimpleContentWatcher;
import ru.sberbank.mobile.core.view.ViewDispatcherUtils;
import ru.sberbank.mobile.viewdispatcher.R;
import ru.sberbank.mobile.viewdispatcher.app.BaseActivity;
import ru.sberbank.mobile.viewdispatcher.di.ViewDispatcherSampleAppComponent;

/**
 * @author QuickNick.
 */
public class PersonsListActivity extends BaseActivity implements IPersonsListViewDispatcher.Listener {

    @Inject
    IPersonsManager mPersonsManager;
    private PersonsListWatcher mPersonsListWatcher;

    private IPersonsListViewDispatcher mPersonsListViewDispatcher;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, PersonsListActivity.class);
        return intent;
    }

    // Activity lifecycle >>>

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ViewDispatcherSampleAppComponent component = getComponent();
        component.inject(this);

        mPersonsListViewDispatcher = new DefaultPersonsListViewDispatcher(this, getSupportFragmentManager(), this);
        ViewDispatcherUtils.setContentView(this, savedInstanceState, mPersonsListViewDispatcher);
        setSupportActionBar(mPersonsListViewDispatcher.getToolbar());

        mPersonsListWatcher = new PersonsListWatcher(savedInstanceState == null);
        getWatcherBundle().add(mPersonsListWatcher);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPersonsListViewDispatcher.destroyView();
        mPersonsListWatcher = null;
    }

    // <<< Activity lifecycle

    // Menu >>>

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.persons_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = super.onOptionsItemSelected(item);
        if (!handled) {
            if (item.getItemId() == R.id.add_person_item) {
                startActivity(EditOrCreatePersonActivity.newIntent(this, new Person()));
                handled = true;
            }
        }
        return handled;
    }

    // <<< Menu

    // IPersonsListViewDispatcher.Listener >>>

    @Override
    public void onRefresh(IPersonsListViewDispatcher sender) {
        mPersonsListWatcher.refresh();
    }

    @Override
    public void onPersonClick(IPersonsListViewDispatcher sender, Person person) {
        startActivity(EditOrCreatePersonActivity.newIntent(this, person));
    }

    // <<< IPersonsListViewDispatcher.Listener

    private class PersonsListWatcher extends SimpleContentWatcher<List<Person>> {

        private boolean mFirstLaunch;

        public PersonsListWatcher(boolean first) {
            super(PersonsListActivity.this);
            mFirstLaunch = first;
        }

        @Override
        protected PendingResult<List<Person>> obtain(boolean force) {
            return mPersonsManager.getPersons();
        }

        @Override
        protected void onLoadStateChanged(IContentWatcher watcher, boolean isLoading) {
            mPersonsListViewDispatcher.setLoading(isLoading, mFirstLaunch);
            if (!mFirstLaunch) {
                mFirstLaunch = false;
            }
        }

        @Override
        protected void onLoaded(List<Person> result) {
            mPersonsListViewDispatcher.setPersons(result);
        }
    }
}
