package ru.sberbank.mobile.viewdispatcher.first;

import android.os.Bundle;
import android.support.annotation.Nullable;

import ru.sberbank.mobile.core.view.ViewDispatcherUtils;
import ru.sberbank.mobile.viewdispatcher.app.BaseActivity;
import ru.sberbank.mobile.viewdispatcher.person.PersonsListActivity;
import ru.sberbank.mobile.viewdispatcher.rates.RatesActivity;

/**
 * @author QuickNick.
 */
public class FirstActivity extends BaseActivity implements IFirstScreenViewDispatcher.Listener {

    private IFirstScreenViewDispatcher mFirstScreenViewDispatcher;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirstScreenViewDispatcher = new DefaultFirstScreenViewDispatcher(this, getSupportFragmentManager(), this);
        ViewDispatcherUtils.setContentView(this, savedInstanceState, mFirstScreenViewDispatcher);
        setSupportActionBar(mFirstScreenViewDispatcher.getToolbar());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mFirstScreenViewDispatcher.destroyView();
    }

    @Override
    public void onCurrenciesRatesClick(IFirstScreenViewDispatcher sender) {
        startActivity(RatesActivity.newIntent(this));
    }

    @Override
    public void onPersonsClick(IFirstScreenViewDispatcher sender) {
        startActivity(PersonsListActivity.newIntent(this));
    }
}
