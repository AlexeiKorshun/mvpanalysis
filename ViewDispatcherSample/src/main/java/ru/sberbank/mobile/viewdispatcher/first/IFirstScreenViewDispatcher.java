package ru.sberbank.mobile.viewdispatcher.first;

import android.support.v7.widget.Toolbar;

import ru.sberbank.mobile.core.view.IViewDispatcher;

/**
 * @author QuickNick.
 */

public interface IFirstScreenViewDispatcher extends IViewDispatcher {

    Toolbar getToolbar();

    interface Listener {

        void onCurrenciesRatesClick(IFirstScreenViewDispatcher sender);

        void onPersonsClick(IFirstScreenViewDispatcher sender);
    }
}
