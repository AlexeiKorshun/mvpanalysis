package ru.sberbank.mobile.viewdispatcher.rates;

import android.support.v7.widget.Toolbar;

import java.util.List;

import ru.sberbank.mobile.common.rate.entity.Rate;
import ru.sberbank.mobile.core.view.IViewDispatcher;

/**
 * @author QuickNick.
 */
public interface IRatesViewDispatcher extends IViewDispatcher {

    void setLoading(boolean loading, boolean firstLaunch);

    void setRates(List<Rate> rates);

    Toolbar getToolbar();

    interface Listener {

        void onRefresh(IRatesViewDispatcher sender);
    }
}
