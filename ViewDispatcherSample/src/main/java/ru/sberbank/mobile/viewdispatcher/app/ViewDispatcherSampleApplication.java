package ru.sberbank.mobile.viewdispatcher.app;

import android.app.Application;

import ru.sberbank.mobile.common.di.CoreLibModule;
import ru.sberbank.mobile.core.di.BaseModelsModule;
import ru.sberbank.mobile.core.di.IHasComponent;
import ru.sberbank.mobile.viewdispatcher.di.DaggerViewDispatcherSampleAppComponent;
import ru.sberbank.mobile.viewdispatcher.di.ViewDispatcherSampleAppComponent;

/**
 * @author QuickNick.
 */
public class ViewDispatcherSampleApplication extends Application implements IHasComponent<ViewDispatcherSampleAppComponent> {

    private ViewDispatcherSampleAppComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerViewDispatcherSampleAppComponent.builder()
                .baseModelsModule(new BaseModelsModule(this))
                .coreLibModule(new CoreLibModule(this))
                .build();
    }

    @Override
    public ViewDispatcherSampleAppComponent getComponent() {
        return mComponent;
    }
}
