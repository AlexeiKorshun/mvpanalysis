package ru.sberbank.mobile.viewdispatcher.rates;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.List;

import ru.sberbank.mobile.common.rate.entity.Rate;
import ru.sberbank.mobile.common.rate.ui.RatesSpinnerAdapter;
import ru.sberbank.mobile.core.format.DecimalFormatter;
import ru.sberbank.mobile.core.utils.LocaleUtils;
import ru.sberbank.mobile.core.view.BaseViewDispatcher;
import ru.sberbank.mobile.viewdispatcher.R;

/**
 * @author QuickNick.
 */

public class DefaultConverterViewDispatcher extends BaseViewDispatcher implements IConverterViewDispatcher {

    private final IConverterViewDispatcher.Listener mListener;

    private ViewGroup mContentContainer;
    private TextInputLayout mSourceAmountInputLayout;
    private EditText mSourceAmountEditText;
    private Spinner mSourceSpinner;
    private TextView mTargetAmountTextView;
    private Spinner mTargetSpinner;
    private Button mConvertButton;
    private ProgressBar mProgressBar;
    private RatesSpinnerAdapter mSourceAdapter;
    private RatesSpinnerAdapter mTargetAdapter;

    public DefaultConverterViewDispatcher(Context context, FragmentManager fragmentManager, IConverterViewDispatcher.Listener listener) {
        super(context, fragmentManager);
        mListener = listener;
    }

    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.converter_activity, container, false);
        mContentContainer = (ViewGroup) view.findViewById(R.id.content_container);
        mSourceAmountInputLayout = (TextInputLayout) view.findViewById(R.id.source_amount_input_layout);
        mSourceAmountEditText = (EditText) view.findViewById(R.id.source_amount_edit_text);
        mSourceSpinner = (Spinner) view.findViewById(R.id.source_currency_spinner);
        mTargetAmountTextView = (TextView) view.findViewById(R.id.target_amount_text_view);
        mTargetSpinner = (Spinner) view.findViewById(R.id.target_currency_spinner);
        mConvertButton = (Button) view.findViewById(R.id.convert_button);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);

        mSourceAdapter = new RatesSpinnerAdapter();
        mTargetAdapter = new RatesSpinnerAdapter();

        mSourceSpinner.setAdapter(mSourceAdapter);
        mTargetSpinner.setAdapter(mTargetAdapter);
        mConvertButton.setOnClickListener(new ConvertButtonClickListener());
        return view;
    }

    @Override
    protected void onDestroyView() {
        mContentContainer = null;
        mSourceAmountInputLayout = null;
        mSourceAmountEditText = null;
        mSourceSpinner = null;
        mTargetAmountTextView = null;
        mTargetSpinner = null;
        mConvertButton = null;
        mProgressBar = null;
        mSourceAdapter = null;
        mTargetAdapter = null;
    }

    @Override
    public void setLoading(boolean loading) {
        if (loading) {
            mContentContainer.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mContentContainer.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setRates(List<Rate> rates) {
        mSourceAdapter.setRates(rates);
        mTargetAdapter.setRates(rates);
    }

    @Override
    public void setSourceAmountError(CharSequence error) {
        mSourceAmountInputLayout.setError(error);
    }

    @Override
    public void showSameRateError() {
        Toast.makeText(mContext, R.string.currencies_must_be_different, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setTargetAmount(BigDecimal targetAmount) {
        mTargetAmountTextView.setText(DecimalFormatter.format(targetAmount, LocaleUtils.getEnglishLocale()));
    }

    @Override
    public Toolbar getToolbar() {
        return (Toolbar) mView.findViewById(R.id.toolbar);
    }

    @Override
    public BigDecimal getSourceAmount() {
        BigDecimal decimal = DecimalFormatter.parseBigDecimal(mSourceAmountEditText.getText().toString(),
                LocaleUtils.getEnglishLocale());
        return decimal;
    }

    @Override
    public Rate getSourceRate() {
        return getRateFromSpinner(mSourceSpinner);
    }

    @Override
    public Rate getTargetRate() {
        return getRateFromSpinner(mTargetSpinner);
    }

    private Rate getRateFromSpinner(Spinner spinner) {
        int selectedIndex = spinner.getSelectedItemPosition();
        Rate rate = (Rate) spinner.getAdapter().getItem(selectedIndex);
        return rate;
    }

    private class ConvertButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            mListener.onConvertClick(DefaultConverterViewDispatcher.this);
        }
    }
}
