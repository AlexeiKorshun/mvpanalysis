package ru.sberbank.mobile.viewdispatcher.first;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ru.sberbank.mobile.core.view.BaseViewDispatcher;
import ru.sberbank.mobile.viewdispatcher.R;

/**
 * @author QuickNick.
 */
public class DefaultFirstScreenViewDispatcher extends BaseViewDispatcher implements IFirstScreenViewDispatcher {

    private final IFirstScreenViewDispatcher.Listener mListener;

    public DefaultFirstScreenViewDispatcher(Context context, FragmentManager fragmentManager, Listener listener) {
        super(context, fragmentManager);
        mListener = listener;
    }

    @Nullable
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.first_activity, container, false);
        view.findViewById(R.id.currencies_button).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mListener.onCurrenciesRatesClick(DefaultFirstScreenViewDispatcher.this);
            }
        });
        view.findViewById(R.id.persons_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onPersonsClick(DefaultFirstScreenViewDispatcher.this);
            }
        });
        return view;
    }

    @Override
    public Toolbar getToolbar() {
        return (Toolbar) mView.findViewById(R.id.toolbar);
    }
}
