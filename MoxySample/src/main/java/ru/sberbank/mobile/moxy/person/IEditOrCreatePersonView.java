package ru.sberbank.mobile.moxy.person;

import com.arellomobile.mvp.MvpView;

/**
 * @author QuickNick
 */

public interface IEditOrCreatePersonView extends MvpView {

    void setLoading(boolean loading);

    void setNameError(boolean error);

    void onSuccess();
}
