package ru.sberbank.mobile.moxy.person;

import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import ru.sberbank.mobile.common.person.IPersonsManager;
import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.observ.ISbolObserverListener;
import ru.sberbank.mobile.core.observ.SbolContentObserver;
import ru.sberbank.mobile.moxy.di.MoxySampleAppComponent;

/**
 * @author QuickNick
 */
@InjectViewState
public class EditOrCreatePersonPresenter extends MvpPresenter<IEditOrCreatePersonView> {

    @Inject
    IPersonsManager mPersonsManager;
    @Inject
    Context mApplicationContext;
    private Person mPerson;

    private ISbolObserverListener mPersonListener = new ISbolObserverListener() {
        @Override
        public void onContentChanged() {
            checkData();
        }
    };
    private SbolContentObserver mContentObserver = null;
    private PendingResult<?> mPendingResult;

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterObserverSafely();
    }

    public void init(MoxySampleAppComponent component, Person person) {
        component.inject(this);
        mPerson = person;
    }

    public Person getPerson() {
        return mPerson;
    }

    public void tryEditOrCreatePerson(String name) {
        if (TextUtils.isEmpty(name)) {
            getViewState().setNameError(true);
        } else {
            getViewState().setNameError(false);
            mPerson.name = name;
            editOrCreatePerson();
        }
    }

    public boolean isUpdatingPerson() {
        return (mPerson.id > 0);
    }

    private void editOrCreatePerson() {
        mPendingResult = isUpdatingPerson() ?
                mPersonsManager.updatePerson(mPerson) : mPersonsManager.addPerson(mPerson);
        Uri uri = mPendingResult.getUri();
        mContentObserver = new SbolContentObserver(mPersonListener);
        mApplicationContext.getContentResolver().registerContentObserver(uri, false, mContentObserver);
        checkData();
    }

    private void checkData() {
        boolean loading = mPendingResult.isLoading();
        getViewState().setLoading(loading);
        if (!mPendingResult.isLoading()) {
            mPendingResult.detach();
            getViewState().onSuccess();
            unregisterObserverSafely();
        }
    }

    private void unregisterObserverSafely() {
        if (mContentObserver != null) {
            mApplicationContext.getContentResolver().unregisterContentObserver(mContentObserver);
            mContentObserver = null;
        }
    }
}
