package ru.sberbank.mobile.moxy.rates;

import android.content.Context;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import ru.sberbank.mobile.common.rate.ICurrencyRateManager;
import ru.sberbank.mobile.common.rate.RateUris;
import ru.sberbank.mobile.common.rate.entity.RatesBundle;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.observ.ISbolObserverListener;
import ru.sberbank.mobile.core.observ.SbolContentObserver;
import ru.sberbank.mobile.moxy.di.MoxySampleAppComponent;

/**
 * @author QuickNick
 */
@InjectViewState
public class RatesPresenter extends MvpPresenter<IRatesView> {

    @Inject
    ICurrencyRateManager mCurrencyRateManager;
    @Inject
    Context mApplicationContext;

    private ISbolObserverListener mRatesListener = new ISbolObserverListener() {
        @Override
        public void onContentChanged() {
            checkData(false);
        }
    };
    private SbolContentObserver mContentObserver = new SbolContentObserver(mRatesListener);

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        mApplicationContext.getContentResolver().registerContentObserver(RateUris.RATES_BUNDLE_URI,
                false, mContentObserver);
    }

    @Override
    public void destroyView(IRatesView view) {
        super.destroyView(view);
        mApplicationContext.getContentResolver().unregisterContentObserver(mContentObserver);
    }

    public void init(MoxySampleAppComponent component) {
        component.inject(this);
    }

    public void loadRates(boolean pullToRefresh) {
        checkData(pullToRefresh);
    }

    private void checkData(boolean pullToRefresh) {
        PendingResult<RatesBundle> result = mCurrencyRateManager.getRatesBundle();
        if (pullToRefresh) {
            result.markAsDirty();
        }
        boolean loading = result.isLoading();
        getViewState().setLoading(loading);
        if (!result.isLoading()) {
            RatesBundle ratesBundle = result.getResult();
            if (ratesBundle.isSuccess()) {
                getViewState().setRates(ratesBundle.getRates());
            } else {
                getViewState().showServerError(ratesBundle.getConnectorStatus());
                result.detach();
            }
        }
    }
}
