package ru.sberbank.mobile.undivided.di;

import javax.inject.Singleton;

import dagger.Component;
import ru.sberbank.mobile.common.di.CoreLibComponent;
import ru.sberbank.mobile.common.di.CoreLibModule;
import ru.sberbank.mobile.core.di.BaseModelsModule;
import ru.sberbank.mobile.undivided.app.BaseActivity;
import ru.sberbank.mobile.undivided.person.EditOrCreatePersonActivity;
import ru.sberbank.mobile.undivided.person.PersonsListActivity;
import ru.sberbank.mobile.undivided.rates.ConverterActivity;
import ru.sberbank.mobile.undivided.rates.RatesActivity;

/**
 * @author QuickNick.
 */
@Singleton
@Component(modules = {BaseModelsModule.class, CoreLibModule.class})
public interface UndividedSampleAppComponent extends CoreLibComponent {

    void inject(BaseActivity activity);

    void inject(RatesActivity activity);

    void inject(ConverterActivity activity);

    void inject(PersonsListActivity activity);

    void inject(EditOrCreatePersonActivity activity);
}
