package ru.sberbank.mobile.undivided.rates;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.List;

import javax.inject.Inject;

import ru.sberbank.mobile.common.rate.ConverterUtils;
import ru.sberbank.mobile.common.rate.ICurrencyRateManager;
import ru.sberbank.mobile.common.rate.entity.Rate;
import ru.sberbank.mobile.common.rate.entity.RatesBundle;
import ru.sberbank.mobile.common.rate.ui.RatesSpinnerAdapter;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.check.SimpleResultChecker2;
import ru.sberbank.mobile.core.format.DecimalFormatter;
import ru.sberbank.mobile.core.observ.IContentWatcher;
import ru.sberbank.mobile.core.observ.UnconditionalStatusedEntityWatcher;
import ru.sberbank.mobile.core.utils.LocaleUtils;
import ru.sberbank.mobile.undivided.R;
import ru.sberbank.mobile.undivided.app.BaseActivity;
import ru.sberbank.mobile.undivided.di.UndividedSampleAppComponent;

/**
 * @author QuickNick.
 */
public class ConverterActivity extends BaseActivity {

    @Inject
    ICurrencyRateManager mCurrencyRateManager;

    private ViewGroup mContentContainer;
    private TextInputLayout mSourceAmountInputLayout;
    private EditText mSourceAmountEditText;
    private Spinner mSourceSpinner;
    private TextView mTargetAmountTextView;
    private Spinner mTargetSpinner;
    private Button mConvertButton;
    private ProgressBar mProgressBar;
    private RatesSpinnerAdapter mSourceAdapter;
    private RatesSpinnerAdapter mTargetAdapter;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, ConverterActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UndividedSampleAppComponent component = getComponent();
        component.inject(this);

        setContentView(R.layout.converter_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        mContentContainer = (ViewGroup) findViewById(R.id.content_container);
        mSourceAmountInputLayout = (TextInputLayout) findViewById(R.id.source_amount_input_layout);
        mSourceAmountEditText = (EditText) findViewById(R.id.source_amount_edit_text);
        mSourceSpinner = (Spinner) findViewById(R.id.source_currency_spinner);
        mTargetAmountTextView = (TextView) findViewById(R.id.target_amount_text_view);
        mTargetSpinner = (Spinner) findViewById(R.id.target_currency_spinner);
        mConvertButton = (Button) findViewById(R.id.convert_button);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mSourceAdapter = new RatesSpinnerAdapter();
        mTargetAdapter = new RatesSpinnerAdapter();

        mSourceSpinner.setAdapter(mSourceAdapter);
        mTargetSpinner.setAdapter(mTargetAdapter);
        mConvertButton.setOnClickListener(new ConvertButtonClickListener());

        getWatcherBundle().add(new RatesBundleWatcher());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mContentContainer = null;
        mSourceAmountInputLayout = null;
        mSourceAmountEditText = null;
        mSourceSpinner = null;
        mTargetAmountTextView = null;
        mTargetSpinner = null;
        mConvertButton = null;
        mProgressBar = null;
        mSourceAdapter = null;
        mTargetAdapter = null;
    }

    private void tryConvert() {
        BigDecimal sourceAmount = getSourceAmount();
        Rate sourceRate = getRateFromSpinner(mSourceSpinner);
        Rate targetRate = getRateFromSpinner(mTargetSpinner);
        if (sourceAmount == null) {
            mSourceAmountInputLayout.setError(getString(R.string.source_amount_error));
        } else {
            mSourceAmountInputLayout.setError(null);
            if (sourceRate.getCurrency() == targetRate.getCurrency()) {
                Toast.makeText(this, R.string.currencies_must_be_different, Toast.LENGTH_LONG).show();
            } else {
                convert(sourceAmount, sourceRate, targetRate);
            }
        }
    }

    private void convert(BigDecimal sourceAmount, Rate sourceRate, Rate targetRate) {
        BigDecimal targetAmount = ConverterUtils.convert(sourceAmount, sourceRate, targetRate);
        mTargetAmountTextView.setText(DecimalFormatter.format(targetAmount, LocaleUtils.getEnglishLocale()));
    }

    private BigDecimal getSourceAmount() {
        BigDecimal decimal = DecimalFormatter.parseBigDecimal(mSourceAmountEditText.getText().toString(),
                LocaleUtils.getEnglishLocale());
        return decimal;
    }

    private Rate getRateFromSpinner(Spinner spinner) {
        int selectedIndex = spinner.getSelectedItemPosition();
        Rate rate = (Rate) spinner.getAdapter().getItem(selectedIndex);
        return rate;
    }

    private class ConvertButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            tryConvert();
        }
    }

    private class RatesBundleWatcher extends UnconditionalStatusedEntityWatcher<RatesBundle> {

        public RatesBundleWatcher() {
            super(ConverterActivity.this,
                    new SimpleResultChecker2(ConverterActivity.this, getSupportFragmentManager()));
        }

        @Override
        protected PendingResult<RatesBundle> obtain(boolean force) {
            return mCurrencyRateManager.getRatesBundle();
        }

        @Override
        protected void onLoadStateChanged(IContentWatcher watcher, boolean isLoading) {
            if (isLoading) {
                mContentContainer.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
            } else {
                mContentContainer.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
            }
        }

        @Override
        protected void onSuccess(IContentWatcher watcher, RatesBundle result, boolean isAlertsAreShowing) {
            List<Rate> rates = result.getRates();
            mSourceAdapter.setRates(rates);
            mTargetAdapter.setRates(rates);
        }

        @Override
        protected void onFailureAndAlertsDismissed(IContentWatcher watcher, RatesBundle result) {
            finish();
        }
    }
}
