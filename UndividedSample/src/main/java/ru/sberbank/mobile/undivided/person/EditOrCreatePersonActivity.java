package ru.sberbank.mobile.undivided.person;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import javax.inject.Inject;

import ru.sberbank.mobile.common.person.IPersonsManager;
import ru.sberbank.mobile.common.person.PersonUris;
import ru.sberbank.mobile.common.person.entity.AddPersonResponse;
import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.check.SimpleResultChecker2;
import ru.sberbank.mobile.core.observ.ConditionalStatusedEntityWatcher;
import ru.sberbank.mobile.core.observ.IContentWatcher;
import ru.sberbank.mobile.core.observ.IContentWatcherCreator;
import ru.sberbank.mobile.core.observ.SimpleContentWatcher;
import ru.sberbank.mobile.undivided.R;
import ru.sberbank.mobile.undivided.app.BaseActivity;
import ru.sberbank.mobile.undivided.di.UndividedSampleAppComponent;

/**
 * @author QuickNick.
 */
public class EditOrCreatePersonActivity extends BaseActivity {

    private static final String EXTRA_PERSON = "ru.sberbank.mobile.intent.extra.PERSON";

    @Inject
    IPersonsManager mPersonsManager;
    private Person mPerson;

    private ViewGroup mContentContainer;
    private TextInputLayout mNameInputLayout;
    private EditText mNameEditText;
    private Button mConfirmButton;
    private ProgressBar mProgressBar;

    public static Intent newIntent(Context context, @NonNull Person person) {
        Intent intent = new Intent(context, EditOrCreatePersonActivity.class);
        intent.putExtra(EXTRA_PERSON, person);
        return intent;
    }

    // Activity lifecycle >>>

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UndividedSampleAppComponent component = getComponent();
        component.inject(this);
        mPerson = (Person) getIntent().getSerializableExtra(EXTRA_PERSON);

        setContentView(R.layout.edit_or_create_person_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        setTitle(isUpdatingPerson() ? R.string.update_person : R.string.create_person);
        mContentContainer = (ViewGroup) findViewById(R.id.content_container);
        mNameInputLayout = (TextInputLayout) findViewById(R.id.name_input_layout);
        mNameEditText = (EditText) findViewById(R.id.name_edit_text);
        mConfirmButton = (Button) findViewById(R.id.confirm_button);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        if (savedInstanceState == null) {
            mNameEditText.setText(mPerson.name);
        };
        mConfirmButton.setOnClickListener(new ConfirmButtonClickListener());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mContentContainer = null;
        mNameInputLayout = null;
        mNameEditText = null;
        mConfirmButton = null;
        mProgressBar = null;
    }

    // <<< Activity lifecycle

    // BaseActivity >>>

    @Override
    protected IContentWatcherCreator createContentWatcherCreator() {
        IContentWatcherCreator creator = new IContentWatcherCreator() {
            @Override
            public IContentWatcher create(Uri uri) {
                IContentWatcher watcher = null;
                if (PersonUris.isAddPersonUri(uri)) {
                    PendingResult<AddPersonResponse> result = getPendingResultRetriever().retrieve(uri);
                    watcher = new AddPersonWatcher(result);
                } else if (PersonUris.isUpdatePersonUri(uri)) {
                    PendingResult<Integer> result = getPendingResultRetriever().retrieve(uri);
                    watcher = new UpdatePersonWatcher(result);
                }
                return watcher;
            }
        };
        return creator;
    }


    // <<< BaseActivity

    private void handleLoadStateChanged(boolean isLoading) {
        if (isLoading) {
            mContentContainer.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mContentContainer.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    private void tryEditOrCreatePerson() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(mNameEditText.getWindowToken(), 0);
        String name = mNameEditText.getText().toString();
        if (TextUtils.isEmpty(name)) {
            mNameInputLayout.setError(getString(R.string.name_error));
        } else {
            mNameInputLayout.setError(null);
            mPerson.name = name;
            editOrCreatePerson();
        }
    }

    private void editOrCreatePerson() {
        if (isUpdatingPerson()) {
            PendingResult<Integer> result = mPersonsManager.updatePerson(mPerson);
            getWatcherBundle().add(new UpdatePersonWatcher(result));
        } else {
            PendingResult<AddPersonResponse> result = mPersonsManager.addPerson(mPerson);
            getWatcherBundle().add(new AddPersonWatcher(result));
        }
    }

    private boolean isUpdatingPerson() {
        return (mPerson.id > 0);
    }

    private class ConfirmButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            tryEditOrCreatePerson();
        }
    }

    private class AddPersonWatcher extends ConditionalStatusedEntityWatcher<AddPersonResponse> {

        public AddPersonWatcher(PendingResult<AddPersonResponse> pendingResult) {
            super(EditOrCreatePersonActivity.this,
                    new SimpleResultChecker2(EditOrCreatePersonActivity.this,
                    getSupportFragmentManager()), pendingResult);
        }

        @Override
        protected void onLoadStateChanged(IContentWatcher watcher, boolean isLoading) {
            handleLoadStateChanged(isLoading);
        }

        @Override
        protected void onSuccess(IContentWatcher watcher, AddPersonResponse result, boolean isAlertsAreShowing) {
            super.onSuccess(watcher, result, isAlertsAreShowing);
            detachResult();
            finish();
        }
    }

    private class UpdatePersonWatcher extends SimpleContentWatcher<Integer> {

        public UpdatePersonWatcher(PendingResult<Integer> result) {
            super(EditOrCreatePersonActivity.this);
            setPendingResult(result);
        }

        @Override
        protected PendingResult<Integer> obtain(boolean force) {
            return getPendingResult();
        }

        @Override
        protected void onLoadStateChanged(IContentWatcher watcher, boolean isLoading) {
            handleLoadStateChanged(isLoading);
        }

        @Override
        protected void onLoaded(Integer result) {
            detachResult();
            finish();
        }

        @Override
        public boolean isConditionalContentWatcher() {
            return true;
        }
    }
}
