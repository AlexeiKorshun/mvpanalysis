package ru.sberbank.mobile.undivided.person;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import javax.inject.Inject;

import ru.sberbank.mobile.common.person.IPersonsManager;
import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.common.person.ui.PersonViewHolder;
import ru.sberbank.mobile.common.person.ui.PersonsAdapter;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.observ.IContentWatcher;
import ru.sberbank.mobile.core.observ.SimpleContentWatcher;
import ru.sberbank.mobile.undivided.R;
import ru.sberbank.mobile.undivided.app.BaseActivity;
import ru.sberbank.mobile.undivided.di.UndividedSampleAppComponent;

/**
 * @author QuickNick.
 */
public class PersonsListActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener,
        PersonViewHolder.OnPersonClickListener {

    @Inject
    IPersonsManager mPersonsManager;
    private PersonsListWatcher mPersonsListWatcher;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private PersonsAdapter mAdapter;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, PersonsListActivity.class);
        return intent;
    }

    // Activity lifecycle >>>

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        UndividedSampleAppComponent component = getComponent();
        component.inject(this);

        setContentView(R.layout.recycler_view_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mAdapter = new PersonsAdapter(this);
        mRecyclerView.setAdapter(mAdapter);

        mPersonsListWatcher = new PersonsListWatcher(savedInstanceState == null);
        getWatcherBundle().add(mPersonsListWatcher);

        mSwipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSwipeRefreshLayout = null;
        mRecyclerView = null;
        mProgressBar = null;
        mAdapter = null;
        mPersonsListWatcher = null;
    }

    // <<< Activity lifecycle

    // Menu >>>

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.persons_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = super.onOptionsItemSelected(item);
        if (!handled) {
            if (item.getItemId() == R.id.add_person_item) {
                startActivity(EditOrCreatePersonActivity.newIntent(this, new Person()));
                handled = true;
            }
        }
        return handled;
    }

    // <<< Menu

    // SwipeRefreshLayout.OnRefreshListener >>>

    @Override
    public void onRefresh() {
        mPersonsListWatcher.refresh();
    }

    // <<< SwipeRefreshLayout.OnRefreshListener

    // PersonViewHolder.OnPersonClickListener >>>

    @Override
    public void onPersonClick(PersonViewHolder sender, Person person) {
        startActivity(EditOrCreatePersonActivity.newIntent(this, person));
    }

    // <<< PersonViewHolder.OnPersonClickListener

    private class PersonsListWatcher extends SimpleContentWatcher<List<Person>> {

        private boolean mFirstLaunch;

        public PersonsListWatcher(boolean first) {
            super(PersonsListActivity.this);
            mFirstLaunch = first;
        }

        @Override
        protected PendingResult<List<Person>> obtain(boolean force) {
            return mPersonsManager.getPersons();
        }

        @Override
        protected void onLoadStateChanged(IContentWatcher watcher, boolean isLoading) {
            mSwipeRefreshLayout.setRefreshing(isLoading);
            if (isLoading) {
                if (mFirstLaunch) {
                    mSwipeRefreshLayout.setVisibility(View.GONE);
                    mProgressBar.setVisibility(View.VISIBLE);
                }
            } else {
                mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                mProgressBar.setVisibility(View.GONE);
                mFirstLaunch = false;
            }
        }

        @Override
        protected void onLoaded(List<Person> result) {
            mAdapter.setPersons(result);
        }
    }
}
