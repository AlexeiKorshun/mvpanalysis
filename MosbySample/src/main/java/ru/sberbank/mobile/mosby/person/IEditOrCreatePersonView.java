package ru.sberbank.mobile.mosby.person;

import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * @author QuickNick
 */

public interface IEditOrCreatePersonView extends MvpView {

    void setLoading(boolean loading);

    void setNameError(boolean error);

    void onSuccess();
}
