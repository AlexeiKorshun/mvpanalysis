package ru.sberbank.mobile.mosby.app;

import android.app.Application;

import ru.sberbank.mobile.common.di.CoreLibModule;
import ru.sberbank.mobile.core.di.BaseModelsModule;
import ru.sberbank.mobile.core.di.IHasComponent;
import ru.sberbank.mobile.mosby.di.DaggerMosbySampleAppComponent;
import ru.sberbank.mobile.mosby.di.MosbySampleAppComponent;

/**
 * @author QuickNick.
 */
public class MosbySampleApplication extends Application implements IHasComponent<MosbySampleAppComponent> {

    private MosbySampleAppComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerMosbySampleAppComponent.builder()
                .baseModelsModule(new BaseModelsModule(this))
                .coreLibModule(new CoreLibModule(this))
                .build();
    }

    @Override
    public MosbySampleAppComponent getComponent() {
        return mComponent;
    }
}
