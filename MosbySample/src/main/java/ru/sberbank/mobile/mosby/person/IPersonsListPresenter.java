package ru.sberbank.mobile.mosby.person;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

/**
 * @author QuickNick
 */
public interface IPersonsListPresenter extends MvpPresenter<IPersonsListView> {

    void loadPersons(boolean pullToRefresh);
}
