package ru.sberbank.mobile.mosby.rates;

import android.content.Context;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import javax.inject.Inject;

import ru.sberbank.mobile.common.rate.ICurrencyRateManager;
import ru.sberbank.mobile.common.rate.RateUris;
import ru.sberbank.mobile.common.rate.entity.RatesBundle;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.observ.ISbolObserverListener;
import ru.sberbank.mobile.core.observ.SbolContentObserver;
import ru.sberbank.mobile.mosby.di.MosbySampleAppComponent;

/**
 * @author QuickNick
 */

public class DefaultRatesPresenter extends MvpBasePresenter<IRatesView> implements IRatesPresenter {

    @Inject
    ICurrencyRateManager mCurrencyRateManager;
    @Inject
    Context mApplicationContext;

    private ISbolObserverListener mRatesListener = new ISbolObserverListener() {
        @Override
        public void onContentChanged() {
            checkData(false);
        }
    };
    private SbolContentObserver mContentObserver = new SbolContentObserver(mRatesListener);

    public DefaultRatesPresenter(MosbySampleAppComponent component) {
        component.inject(this);
    }

    @Override
    public void attachView(IRatesView view) {
        super.attachView(view);
        mApplicationContext.getContentResolver().registerContentObserver(RateUris.RATES_BUNDLE_URI,
                false, mContentObserver);
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        mApplicationContext.getContentResolver().unregisterContentObserver(mContentObserver);
    }

    @Override
    public void loadRates(boolean pullToRefresh) {
        checkData(pullToRefresh);
    }

    private void checkData(boolean pullToRefresh) {
        if (isViewAttached()) {
            PendingResult<RatesBundle> result = mCurrencyRateManager.getRatesBundle();
            if (pullToRefresh) {
                result.markAsDirty();
            }
            boolean loading = result.isLoading();
            getView().setLoading(loading);
            if (!result.isLoading()) {
                RatesBundle ratesBundle = result.getResult();
                if (ratesBundle.isSuccess()) {
                    getView().setRates(ratesBundle.getRates());
                } else {
                    getView().showServerError(ratesBundle.getConnectorStatus());
                    result.detach();
                }
            }
        }
    }
}
