package ru.sberbank.mobile.mosby.rates;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.math.BigDecimal;
import java.util.List;

import ru.sberbank.mobile.common.rate.entity.Rate;
import ru.sberbank.mobile.core.network.ConnectorStatus;

/**
 * @author QuickNick
 */
public interface IConverterView extends MvpView {

    void setLoading(boolean loading);

    void setRates(List<Rate> rates);

    void showServerError(ConnectorStatus status);

    void setTargetAmount(BigDecimal targetAmount);

    void showSourceAmountError(boolean error);

    void showSameRatesError();
}
