package ru.sberbank.mobile.mosby.person;

import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.List;

import ru.sberbank.mobile.common.person.entity.Person;

/**
 * @author QuickNick
 */
public interface IPersonsListView extends MvpView {

    void setLoading(boolean loading);

    void setPersons(List<Person> persons);
}
