package ru.sberbank.mobile.mosby.di;

import javax.inject.Singleton;

import dagger.Component;
import ru.sberbank.mobile.common.di.CoreLibComponent;
import ru.sberbank.mobile.common.di.CoreLibModule;
import ru.sberbank.mobile.core.di.BaseModelsModule;
import ru.sberbank.mobile.mosby.person.DefaultEditOrCreatePersonPresenter;
import ru.sberbank.mobile.mosby.person.DefaultPersonsListPresenter;
import ru.sberbank.mobile.mosby.rates.DefaultConverterPresenter;
import ru.sberbank.mobile.mosby.rates.DefaultRatesPresenter;

/**
 * @author QuickNick.
 */
@Singleton
@Component(modules = {BaseModelsModule.class, CoreLibModule.class})
public interface MosbySampleAppComponent extends CoreLibComponent {

    void inject(DefaultRatesPresenter presenter);

    void inject(DefaultConverterPresenter presenter);

    void inject(DefaultPersonsListPresenter presenter);

    void inject(DefaultEditOrCreatePersonPresenter presenter);
}
