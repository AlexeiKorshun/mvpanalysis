package ru.sberbank.mobile.mosby.person;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.mosby.R;
import ru.sberbank.mobile.mosby.app.BaseActivity;
import ru.sberbank.mobile.mosby.di.MosbySampleAppComponent;

/**
 * @author QuickNick.
 */
public class EditOrCreatePersonActivity extends BaseActivity<IEditOrCreatePersonView, IEditOrCreatePersonPresenter>
        implements IEditOrCreatePersonView {

    private static final String EXTRA_PERSON = "ru.sberbank.mobile.intent.extra.PERSON";

    private ViewGroup mContentContainer;
    private TextInputLayout mNameInputLayout;
    private EditText mNameEditText;
    private Button mConfirmButton;
    private ProgressBar mProgressBar;

    public static Intent newIntent(Context context, @NonNull Person person) {
        Intent intent = new Intent(context, EditOrCreatePersonActivity.class);
        intent.putExtra(EXTRA_PERSON, person);
        return intent;
    }

    // Activity lifecycle >>>

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_or_create_person_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        setTitle(getPresenter().isUpdatingPerson() ? R.string.update_person : R.string.create_person);
        mContentContainer = (ViewGroup) findViewById(R.id.content_container);
        mNameInputLayout = (TextInputLayout) findViewById(R.id.name_input_layout);
        mNameEditText = (EditText) findViewById(R.id.name_edit_text);
        mConfirmButton = (Button) findViewById(R.id.confirm_button);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        if (savedInstanceState == null) {
            mNameEditText.setText(getPresenter().getPerson().name);
        }
        ;
        mConfirmButton.setOnClickListener(new ConfirmButtonClickListener());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mContentContainer = null;
        mNameInputLayout = null;
        mNameEditText = null;
        mConfirmButton = null;
        mProgressBar = null;
    }

    // <<< Activity lifecycle

    @NonNull
    @Override
    public IEditOrCreatePersonPresenter createPresenter() {
        return new DefaultEditOrCreatePersonPresenter((MosbySampleAppComponent) getComponent(),
                (Person) getIntent().getSerializableExtra(EXTRA_PERSON));
    }

    @Override
    public void setLoading(boolean loading) {
        if (loading) {
            mContentContainer.setVisibility(View.GONE);
            mProgressBar.setVisibility(View.VISIBLE);
        } else {
            mContentContainer.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setNameError(boolean error) {
        mNameInputLayout.setError(error ? getString(R.string.name_error) : null);
    }

    @Override
    public void onSuccess() {
        finish();
    }

    private void tryEditOrCreatePerson() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(mNameEditText.getWindowToken(), 0);
        String name = mNameEditText.getText().toString();
        getPresenter().tryEditOrCreatePerson(name);
    }

    private class ConfirmButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            tryEditOrCreatePerson();
        }
    }
}
