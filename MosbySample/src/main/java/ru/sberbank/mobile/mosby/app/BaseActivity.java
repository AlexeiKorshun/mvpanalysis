package ru.sberbank.mobile.mosby.app;

import com.hannesdorfmann.mosby3.mvp.MvpActivity;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import ru.sberbank.mobile.core.di.IHasComponent;

/**
 * @author QuickNick
 */

public abstract class BaseActivity<V extends MvpView, P extends MvpPresenter<V>>
        extends MvpActivity<V, P> {

    protected <T> T getComponent() {
        IHasComponent<T> hasComponent = (IHasComponent<T>) getApplication();
        return hasComponent.getComponent();
    }
}
