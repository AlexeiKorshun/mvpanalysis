package ru.sberbank.mobile.mosby.person;

import android.content.Context;

import com.hannesdorfmann.mosby3.mvp.MvpBasePresenter;

import java.util.List;

import javax.inject.Inject;

import ru.sberbank.mobile.common.person.IPersonsManager;
import ru.sberbank.mobile.common.person.PersonUris;
import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.observ.ISbolObserverListener;
import ru.sberbank.mobile.core.observ.SbolContentObserver;
import ru.sberbank.mobile.mosby.di.MosbySampleAppComponent;

/**
 * @author QuickNick
 */

public class DefaultPersonsListPresenter extends MvpBasePresenter<IPersonsListView> implements IPersonsListPresenter {

    @Inject
    IPersonsManager mPersonsManager;
    @Inject
    Context mApplicationContext;

    private ISbolObserverListener mPersonsListener = new ISbolObserverListener() {
        @Override
        public void onContentChanged() {
            checkData(false);
        }
    };
    private SbolContentObserver mContentObserver = new SbolContentObserver(mPersonsListener);

    public DefaultPersonsListPresenter(MosbySampleAppComponent component) {
        component.inject(this);
    }

    @Override
    public void attachView(IPersonsListView view) {
        super.attachView(view);
        mApplicationContext.getContentResolver().registerContentObserver(PersonUris.PERSONS_URI,
                false, mContentObserver);
    }

    @Override
    public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        mApplicationContext.getContentResolver().unregisterContentObserver(mContentObserver);
    }

    @Override
    public void loadPersons(boolean pullToRefresh) {
        checkData(pullToRefresh);
    }

    private void checkData(boolean pullToRefresh) {
        if (isViewAttached()) {
            PendingResult<List<Person>> result = mPersonsManager.getPersons();
            if (pullToRefresh) {
                result.markAsDirty();
            }
            boolean loading = result.isLoading();
            getView().setLoading(loading);
            if (!result.isLoading()) {
                List<Person> persons = result.getResult();
                getView().setPersons(persons);
            }
        }
    }
}
