package ru.sberbank.mobile.mosby.person;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import java.util.List;

import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.common.person.ui.PersonViewHolder;
import ru.sberbank.mobile.common.person.ui.PersonsAdapter;
import ru.sberbank.mobile.mosby.R;
import ru.sberbank.mobile.mosby.app.BaseActivity;
import ru.sberbank.mobile.mosby.di.MosbySampleAppComponent;

/**
 * @author QuickNick.
 */
public class PersonsListActivity extends BaseActivity<IPersonsListView, IPersonsListPresenter>
        implements IPersonsListView, SwipeRefreshLayout.OnRefreshListener, PersonViewHolder.OnPersonClickListener {

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    private PersonsAdapter mAdapter;
    private boolean mFirstLaunch;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, PersonsListActivity.class);
        return intent;
    }

    // Activity lifecycle >>>

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirstLaunch = (savedInstanceState == null);

        setContentView(R.layout.recycler_view_activity);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);

        mAdapter = new PersonsAdapter(this);
        mRecyclerView.setAdapter(mAdapter);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        getPresenter().loadPersons(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSwipeRefreshLayout = null;
        mRecyclerView = null;
        mProgressBar = null;
        mAdapter = null;
    }

    // <<< Activity lifecycle

    // Menu >>>

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.persons_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = super.onOptionsItemSelected(item);
        if (!handled) {
            if (item.getItemId() == R.id.add_person_item) {
                startActivity(EditOrCreatePersonActivity.newIntent(this, new Person()));
                handled = true;
            }
        }
        return handled;
    }

    // <<< Menu

    @NonNull
    @Override
    public IPersonsListPresenter createPresenter() {
        return new DefaultPersonsListPresenter((MosbySampleAppComponent) getComponent());
    }

    // IPersonsListView >>>

    @Override
    public void setLoading(boolean loading) {
        mSwipeRefreshLayout.setRefreshing(loading);
        if (loading) {
            if (mFirstLaunch) {
                mSwipeRefreshLayout.setVisibility(View.GONE);
                mProgressBar.setVisibility(View.VISIBLE);
            }
        } else {
            mFirstLaunch = false;
            mSwipeRefreshLayout.setVisibility(View.VISIBLE);
            mProgressBar.setVisibility(View.GONE);
        }
    }

    @Override
    public void setPersons(List<Person> persons) {
        mAdapter.setPersons(persons);
    }

    // <<< IPersonsListView

    // SwipeRefreshLayout.OnRefreshListener >>>

    @Override
    public void onRefresh() {
        getPresenter().loadPersons(true);
    }

    // <<< SwipeRefreshLayout.OnRefreshListener

    // PersonViewHolder.OnPersonClickListener >>>

    @Override
    public void onPersonClick(PersonViewHolder sender, Person person) {
        startActivity(EditOrCreatePersonActivity.newIntent(this, person));
    }

    // <<< PersonViewHolder.OnPersonClickListener
}
