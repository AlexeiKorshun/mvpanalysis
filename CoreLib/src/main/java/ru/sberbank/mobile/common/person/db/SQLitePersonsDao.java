package ru.sberbank.mobile.common.person.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import ru.sberbank.mobile.common.person.entity.Person;

/**
 * @author QuickNick.
 */

public class SQLitePersonsDao implements IPersonsDao {

    private final SQLiteOpenHelper mHelper;

    public SQLitePersonsDao(SQLiteOpenHelper helper) {
        mHelper = helper;
    }

    @Override
    public synchronized List<Person> getPersons() {
        List<Person> persons = new ArrayList<>();
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.query(SQLitePersonsHelper.TABLE_PERSONS, null, null, null, null, null, null);
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    persons.add(personFromCursor(cursor));
                    cursor.moveToNext();
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }
        return persons;
    }

    @Override
    public synchronized long addPerson(Person person) {
        long id = -1;
        SQLiteDatabase db = mHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = contentValuesFromPerson(person);
            id = db.insert(SQLitePersonsHelper.TABLE_PERSONS, null, values);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }
        return id;
    }

    @Override
    public synchronized int updatePerson(Person person) {
        int updatedRows = -1;
        SQLiteDatabase db = mHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            ContentValues values = contentValuesFromPerson(person);
            String whereClause = PersonsContract.Persons._ID + " = ?";
            String[] whereArgs = {String.valueOf(person.id)};
            updatedRows = db.update(SQLitePersonsHelper.TABLE_PERSONS, values, whereClause, whereArgs);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }
        return updatedRows;
    }

    @Override
    public synchronized int deletePerson(Person person) {
        int deletedRows = -1;
        SQLiteDatabase db = mHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            String whereClause = PersonsContract.Persons._ID + " = ?";
            String[] whereArgs = {String.valueOf(person.id)};
            deletedRows = db.delete(SQLitePersonsHelper.TABLE_PERSONS, whereClause, whereArgs);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
            db.close();
        }
        return deletedRows;
    }

    private static Person personFromCursor(Cursor cursor) {
        Person person = new Person();
        person.id = getLong(cursor, PersonsContract.Persons._ID);
        person.name = getString(cursor, PersonsContract.Persons.NAME);
        return person;
    }

    private static ContentValues contentValuesFromPerson(Person person) {
        ContentValues values = new ContentValues();
        values.put(PersonsContract.Persons.NAME, person.name);
        return values;
    }

    private static long getLong(Cursor cursor, String columnName) {
        return cursor.getLong(cursor.getColumnIndex(columnName));
    }

    private static String getString(Cursor cursor, String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }
}
