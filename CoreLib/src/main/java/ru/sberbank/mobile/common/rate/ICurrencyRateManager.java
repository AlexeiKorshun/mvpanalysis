package ru.sberbank.mobile.common.rate;

import android.net.Uri;

import ru.sberbank.mobile.common.rate.entity.RatesBundle;
import ru.sberbank.mobile.core.async.PendingResult;

/**
 * Менеджер курсов валют
 *
 * @author QuickNick
 */
public interface ICurrencyRateManager {

    PendingResult<RatesBundle> getRatesBundle();
}
