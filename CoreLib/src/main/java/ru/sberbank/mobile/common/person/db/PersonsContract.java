package ru.sberbank.mobile.common.person.db;

import android.provider.BaseColumns;

/**
 * @author QuickNick.
 */

public class PersonsContract {

    public static interface PersonColumns {
        String NAME = "name";
    }

    public static class Persons implements BaseColumns, PersonColumns {
       public static final String[] ALL_COLUMNS = new String[] {_ID, NAME};
    }
}
