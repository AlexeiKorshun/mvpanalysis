package ru.sberbank.mobile.common.person.entity;

import com.google.common.base.Objects;

import java.io.Serializable;

/**
 * @author QuickNick.
 */
public class Person implements Serializable {

    public long id;
    public String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Person)) {
            return false;
        }
        Person person = (Person) o;
        return id == person.id &&
                Objects.equal(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id, name);
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .toString();
    }
}
