package ru.sberbank.mobile.common.person.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author QuickNick.
 */
public class SQLitePersonsHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "persons.db";
    public static final int VERSION = 1;

    public static final String TABLE_PERSONS = "persons";

    public SQLitePersonsHelper(Context context) {
        this(context, DATABASE_NAME);
    }

    public SQLitePersonsHelper(Context context, String dbName) {
        super(context, dbName, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            createPersonsTable(db);
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private static void createPersonsTable(SQLiteDatabase db) {
        StringBuilder query = new StringBuilder();
        query.append("CREATE TABLE ").append(TABLE_PERSONS).append(" (")
                .append(PersonsContract.Persons._ID).append(" INTEGER PRIMARY KEY, ")
                .append(PersonsContract.Persons.NAME).append(" TEXT NOT NULL")
                .append(");");
        db.execSQL(query.toString());
    }
}
