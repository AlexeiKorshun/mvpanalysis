package ru.sberbank.mobile.common.person;

import android.net.Uri;

import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.utils.UriUtils;

/**
 * @author QuickNick.
 */

public class PersonUris {

    public static final Uri PERSONS_URI = Uri.parse("sample-app://persons");
    private static final String OPERATION = "operation";
    private static final String ADD = "add";
    private static final String UPDATE = "update";
    private static final String DELETE = "delete";
    private static final String ID = "id";
    private static final String HASH = "hash";

    public static Uri getAddPersonUri(Person person) {
        int hash = person.hashCode();
        Uri uri = PERSONS_URI.buildUpon().appendQueryParameter(OPERATION, ADD)
                .appendQueryParameter(HASH, String.valueOf(hash))
                .build();
        return uri;
    }

    public static Uri getUpdatePersonUri(Person person) {
        Uri uri = PERSONS_URI.buildUpon().appendQueryParameter(OPERATION, UPDATE)
                .appendQueryParameter(ID, String.valueOf(person.id))
                .build();
        return uri;
    }

    public static Uri getDeletePersonUri(Person person) {
        Uri uri = PERSONS_URI.buildUpon().appendQueryParameter(OPERATION, DELETE)
                .appendQueryParameter(ID, String.valueOf(person.id))
                .build();
        return uri;
    }

    public static boolean isAddPersonUri(Uri uri) {
        boolean result = UriUtils.isEqualOrNestedUri(PERSONS_URI, uri)
                && ADD.equals(uri.getQueryParameter(OPERATION));
        return result;
    }

    public static boolean isUpdatePersonUri(Uri uri) {
        boolean result = UriUtils.isEqualOrNestedUri(PERSONS_URI, uri)
                && UPDATE.equals(uri.getQueryParameter(OPERATION));
        return result;
    }
}
