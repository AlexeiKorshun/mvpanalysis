package ru.sberbank.mobile.common.rate;

import android.net.Uri;

/**
 * @author QuickNick.
 */
public class RateUris {
    public static final Uri RATES_BUNDLE_URI = Uri.parse("sample-app://rates?operation=list");
}
