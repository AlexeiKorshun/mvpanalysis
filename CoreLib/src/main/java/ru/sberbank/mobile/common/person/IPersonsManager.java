package ru.sberbank.mobile.common.person;

import java.util.List;

import ru.sberbank.mobile.common.person.entity.AddPersonResponse;
import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.async.PendingResult;

/**
 * Менеджер контактов, предоставляющий CRUD-операции над ними.
 *
 * @author QuickNick
 */
public interface IPersonsManager {

    PendingResult<List<Person>> getPersons();

    PendingResult<AddPersonResponse> addPerson(Person person);

    PendingResult<Integer> updatePerson(Person person);

    PendingResult<Integer> deletePerson(Person person);
}
