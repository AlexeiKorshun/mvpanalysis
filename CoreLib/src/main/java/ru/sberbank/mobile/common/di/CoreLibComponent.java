package ru.sberbank.mobile.common.di;

import ru.sberbank.mobile.common.person.IPersonsManager;
import ru.sberbank.mobile.common.rate.ICurrencyRateManager;
import ru.sberbank.mobile.core.observ.IPendingResultRetriever;

/**
 * @author QuickNick.
 */
public interface CoreLibComponent {

    IPendingResultRetriever getPendingResultRetriever();

    IPersonsManager getPersonsManager();

    ICurrencyRateManager getCurrencyRateManager();
}
