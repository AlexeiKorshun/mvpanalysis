package ru.sberbank.mobile.common.person.db;

import java.util.List;

import ru.sberbank.mobile.common.person.entity.Person;

/***
 * @author QuickNick.
 */
public interface IPersonsDao {

    List<Person> getPersons();

    long addPerson(Person person);

    int updatePerson(Person person);

    int deletePerson(Person person);
}
