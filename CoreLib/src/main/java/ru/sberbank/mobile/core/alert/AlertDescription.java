package ru.sberbank.mobile.core.alert;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;

import com.google.common.base.Objects;

import ru.sberbank.mobile.core.bean.text.TextWrapper;

/**
 * @author QuickNick
 */
public class AlertDescription implements Parcelable {

    public static final Parcelable.Creator<AlertDescription> CREATOR = new AlertDescriptionParcelableCreator();

    private TextWrapper mTitle;
    private TextWrapper mMessage;
    private ButtonAction mPositiveButton;
    private ButtonAction mNeutralButton;
    private ButtonAction mNegativeButton;
    private AlertAction mOnDismissAction;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AlertDescription)) {
            return false;
        }
        AlertDescription that = (AlertDescription) o;
        boolean result = Objects.equal(mTitle, that.mTitle) &&
                Objects.equal(mMessage, that.mMessage) &&
                Objects.equal(mPositiveButton, that.mPositiveButton) &&
                Objects.equal(mNeutralButton, that.mNeutralButton) &&
                Objects.equal(mNegativeButton, that.mNegativeButton) &&
                Objects.equal(mOnDismissAction, that.mOnDismissAction);
        return result;
    }

    @Override
    public int hashCode() {
        int hashCode = Objects.hashCode(mTitle, mMessage, mPositiveButton, mNeutralButton, mNegativeButton);
        hashCode = 31 * hashCode + (mOnDismissAction != null ? mOnDismissAction.hashCode() : 0);
        return hashCode;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("mTitle", mTitle)
                .add("mMessage", mMessage)
                .add("mPositiveButton", mPositiveButton)
                .add("mNeutralButton", mNeutralButton)
                .add("mNegativeButton", mNegativeButton)
                .add("mOnDismissAction", mOnDismissAction)
                .toString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mTitle, flags);
        dest.writeParcelable(mMessage, flags);
        dest.writeParcelable(mPositiveButton, flags);
        dest.writeParcelable(mNeutralButton, flags);
        dest.writeParcelable(mNegativeButton, flags);
        dest.writeSerializable(mOnDismissAction);
    }

    @Nullable
    public TextWrapper getTitle() {
        return mTitle;
    }

    public void setTitle(@Nullable TextWrapper title) {
        this.mTitle = title;
    }

    public void setTitle(@StringRes int textResId) {
        setTitle(new TextWrapper(textResId));
    }

    public void setTitle(@NonNull String text) {
        setTitle(new TextWrapper(text));
    }

    @Nullable
    public TextWrapper getMessage() {
        return mMessage;
    }

    public void setMessage(@Nullable TextWrapper message) {
        this.mMessage = message;
    }

    public void setMessage(@StringRes int textResId) {
        setMessage(new TextWrapper(textResId));
    }

    public void setMessage(@NonNull String text) {
        setMessage(new TextWrapper(text));
    }

    @Nullable
    public ButtonAction getPositiveButton() {
        return mPositiveButton;
    }

    public void setPositiveButton(@Nullable ButtonAction positiveButton) {
        this.mPositiveButton = positiveButton;
    }

    @Nullable
    public ButtonAction getNeutralButton() {
        return mNeutralButton;
    }

    public void setNeutralButton(@Nullable ButtonAction neutralButton) {
        this.mNeutralButton = neutralButton;
    }

    @Nullable
    public ButtonAction getNegativeButton() {
        return mNegativeButton;
    }

    public void setNegativeButton(@Nullable ButtonAction negativeButton) {
        this.mNegativeButton = negativeButton;
    }

    @Nullable
    public AlertAction getOnDismissAction() {
        return mOnDismissAction;
    }

    public void setOnDismissAction(@Nullable AlertAction onDismissAction) {
        this.mOnDismissAction = onDismissAction;
    }

    private static class AlertDescriptionParcelableCreator implements Parcelable.Creator<AlertDescription> {

        @Override
        public AlertDescription createFromParcel(Parcel source) {
            AlertDescription description = new AlertDescription();
            ClassLoader loader = getClass().getClassLoader();
            description.mTitle = source.readParcelable(loader);
            description.mMessage = source.readParcelable(loader);
            description.mPositiveButton = source.readParcelable(loader);
            description.mNeutralButton = source.readParcelable(loader);
            description.mNegativeButton = source.readParcelable(loader);
            description.mOnDismissAction = (AlertAction) source.readSerializable();
            return description;
        }

        @Override
        public AlertDescription[] newArray(int size) {
            return new AlertDescription[size];
        }
    }

    public static class ButtonAction implements Parcelable {

        public static final Parcelable.Creator<ButtonAction> CREATOR = new ActionParcelableCreator();

        private final TextWrapper mLabel;
        private AlertAction mAction;

        public ButtonAction(@NonNull TextWrapper label, @Nullable AlertAction action) {
            mLabel = label;
            mAction = action;
        }

        public ButtonAction(@StringRes int textResId, @Nullable AlertAction action) {
            this(new TextWrapper(textResId), action);
        }

        public ButtonAction(@NonNull String text, @Nullable AlertAction action) {
            this(new TextWrapper(text), action);
        }

        public ButtonAction(@NonNull Parcel source) {
            ClassLoader loader = getClass().getClassLoader();
            mLabel = source.readParcelable(loader);
            mAction = (AlertAction) source.readSerializable();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (!(o instanceof ButtonAction)) {
                return false;
            }
            ButtonAction that = (ButtonAction) o;
            return Objects.equal(mLabel, that.mLabel) &&
                    Objects.equal(mAction, that.mAction);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(mLabel, mAction);
        }

        @Override
        public String toString() {
            return Objects.toStringHelper(this)
                    .add("mLabel", mLabel)
                    .add("mAction", mAction)
                    .toString();
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeParcelable(mLabel, flags);
            dest.writeSerializable(mAction);
        }

        @NonNull
        public TextWrapper getLabel() {
            return mLabel;
        }

        @Nullable
        public AlertAction getAction() {
            return mAction;
        }

        private static class ActionParcelableCreator implements Parcelable.Creator<ButtonAction> {

            @Override
            public ButtonAction createFromParcel(Parcel source) {
                return new ButtonAction(source);
            }

            @Override
            public ButtonAction[] newArray(int size) {
                return new ButtonAction[size];
            }
        }
    }
}
