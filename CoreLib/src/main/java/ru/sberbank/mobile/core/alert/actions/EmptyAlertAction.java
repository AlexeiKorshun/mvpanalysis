package ru.sberbank.mobile.core.alert.actions;

import android.support.annotation.NonNull;

import ru.sberbank.mobile.core.alert.AlertAction;
import ru.sberbank.mobile.core.alert.AlertDialogFragment;

/**
 * Пустое действие для алерта.
 *
 * @author Дмитрий Соколов
 */
public class EmptyAlertAction extends AlertAction {
    @Override
    public void onEvent(@NonNull AlertDialogFragment sender) {
        // Do nothing
    }
}
