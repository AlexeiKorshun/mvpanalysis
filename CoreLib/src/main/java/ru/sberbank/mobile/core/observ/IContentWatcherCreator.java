package ru.sberbank.mobile.core.observ;

import android.net.Uri;

/**
 * Created by krygin on 30.11.16.
 */

public interface IContentWatcherCreator {
    IContentWatcher create(Uri uri);
}
