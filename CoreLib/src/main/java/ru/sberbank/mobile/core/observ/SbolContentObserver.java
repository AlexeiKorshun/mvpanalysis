package ru.sberbank.mobile.core.observ;

import android.database.ContentObserver;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;

/**
 * @author Alex Gontarenko
 *
 */
public class SbolContentObserver extends ContentObserver {

    private final ISbolObserverListener mListener;

    public SbolContentObserver(@NonNull ISbolObserverListener listener) {
        super(new Handler());
        mListener = listener;
    }

    @Override
    public boolean deliverSelfNotifications() {
        return true;
    }

    @Override
    public void onChange(boolean selfChange) {
        this.onChange(selfChange, null);
    }
    @Override
    public void onChange(boolean selfChange, Uri uri) {
        if (mListener != null) {
            mListener.onContentChanged();
        }
    }
}
