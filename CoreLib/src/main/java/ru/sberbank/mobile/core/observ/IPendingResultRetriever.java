package ru.sberbank.mobile.core.observ;

import android.net.Uri;

import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.bean.operation.StatusedEntity;

/**
 * Created by krygin on 01.12.16.
 */

public interface IPendingResultRetriever {
    <T> PendingResult<T> retrieve(Uri uri);
}
