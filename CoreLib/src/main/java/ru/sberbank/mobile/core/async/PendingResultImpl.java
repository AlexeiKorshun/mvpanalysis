package ru.sberbank.mobile.core.async;

import android.net.Uri;
import android.support.annotation.Nullable;

import com.google.common.base.Objects;

import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import ru.sberbank.mobile.core.log.Logger;

/**
 * @author QuickNick
 */
final class PendingResultImpl<T> implements PendingResult<T>, NoExceptionCallable<T> {

    private static final String TAG = "PendingResultImpl";

    private final Uri mUri;
    private final NoExceptionCallable<T> mCallable;
    private final ResultDispatcher<T> mResultDispatcher;

    private volatile Content<T> mContentResult;
    private volatile Future<T> mFuture;
    private volatile boolean mLoading;
    private volatile boolean mDirty;

    PendingResultImpl(Uri uri, NoExceptionCallable<T> callable, ResultDispatcher<T> resultDispatcher) {
        mUri = uri;
        mCallable = callable;
        mResultDispatcher = resultDispatcher;
        mDirty = true;
        mLoading = false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isDirty() {
        return mDirty;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void markAsDirty() {
        Future<T> future = mFuture;
        if (future != null && !future.isDone()) {
            future.cancel(true);
        }
        mDirty = true;
        mLoading = true;
        mResultDispatcher.onBecomeDirty(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isLoading() {
        return mLoading;
    }

    void setLoading(boolean value) {
        mLoading = value;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isReady() {
        return mContentResult != null && !mDirty;
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public T getResult() {
        Content<T> content = mContentResult;
        T result = null;
        if (content != null) {
            result = content.get();
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public T awaitAndGetResult() {
        //fixme скорее всего тут NPE
        Future<T> localFuture = mFuture;
        T localResult = null;
        if (localFuture != null) {
            try {
                localResult = localFuture.get();
                mDirty = false;
            } catch (InterruptedException e) {
            } catch (ExecutionException e) {
            } catch (CancellationException e) {
            }
        }
        return localResult;
    }

    @Override
    public void detach() {
        mResultDispatcher.detachMe(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Uri getUri() {
        return mUri;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized T call() {
        try {
            mContentResult = new Content<>(mCallable.call());
            mDirty = false;
            mLoading = false;
            mResultDispatcher.onLoaded(this);
            return mContentResult.get();
        } catch (RuntimeException ex) {
            Logger.e(TAG, "exception", ex);
            throw ex;
        }
    }

    public void setResult(T result) {
        mContentResult = new Content<>(result);
    }

    public void setFuture(Future<T> future) {
        //setLoadingState(true);
        mFuture = future;
    }

    public Future<T> getFuture() {
        return mFuture;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("mUri", mUri)
                .add("mCallable", mCallable)
                .add("mResultDispatcher", mResultDispatcher)
                .add("mContentResult", mContentResult)
                .add("mFuture", mFuture)
                .add("mLoading", mLoading)
                .add("mDirty", mDirty)
                .toString();
    }
}
