package ru.sberbank.mobile.core.check;

import ru.sberbank.mobile.core.alert.AlertAction;

/**
 * Параметры для чекера результатов.
 * <ol>
 *     <li>afterShowAction -- некоторое действие, которое следует выполнить после дисмисса диалога,
 *     который показывается по итогам проверки результата операции.</li>
 *     <li>includeMessagesWithElementId -- следует ли включать в текст диалога сообщения
 *     с идентификаторами элементов? (по-хорошему, их следует демонстрировать непосредственно на вьюхе, под TextInputLayout-ами)</li>
 * </ol>
 */
public class CheckOptions {

    public AlertAction afterShowAction;
    public boolean includeMessagesWithElementId;

    public CheckOptions() {
        this(null, false);
    }

    public CheckOptions(AlertAction afterShowAction, boolean includeMessagesWithElementId) {
        this.afterShowAction = afterShowAction;
        this.includeMessagesWithElementId = includeMessagesWithElementId;
    }
}
