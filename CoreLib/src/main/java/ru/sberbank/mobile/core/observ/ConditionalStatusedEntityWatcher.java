package ru.sberbank.mobile.core.observ;

import android.content.Context;

import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.bean.operation.StatusedEntity;
import ru.sberbank.mobile.core.check.IResultChecker;

/**
 * Created by krygin on 01.12.16.
 */

public abstract class ConditionalStatusedEntityWatcher<T extends StatusedEntity> extends AbstractStatusedEntityWatcher<T> implements IContentWatcher {

    public ConditionalStatusedEntityWatcher(Context context, IResultChecker<? super T> resultChecker) {
        this(context, resultChecker, null);
    }

    public ConditionalStatusedEntityWatcher(Context context, IResultChecker<? super T> resultChecker, PendingResult<T> pendingResult) {
        super(context, resultChecker, pendingResult);
    }

    @Override
    protected PendingResult<T> obtain(boolean force) {
        return getPendingResult();
    }

    @Override
    public boolean isConditionalContentWatcher() {
        return true;
    }
}
