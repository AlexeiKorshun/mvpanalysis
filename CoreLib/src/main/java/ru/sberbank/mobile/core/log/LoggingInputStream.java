package ru.sberbank.mobile.core.log;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * User: sbt-bubnov-vy
 * Date: 08.10.12
 * Time: 18:07
 */
public class LoggingInputStream extends InputStream {
    private final static FooLogger FOO_LOGGER = new FooLogger();

    private final InputStream mSource;
    private final LoggingInputStreamLogger mLogger;

    public LoggingInputStream(InputStream source, LoggingInputStreamLogger logger) {
        this.mSource = source;
        if (logger == null) {
            logger = FOO_LOGGER;
        }
        this.mLogger = logger;
    }

    @Override
    public int read() throws IOException {
        int read = mSource.read();
        mLogger.log(read);
        return read;
    }


    @Override
    public int read(byte[] b) throws IOException {
        int read = super.read(b);
        mLogger.log(b, 0, b.length);
        return read;
    }


    @Override
    public int read(byte[] b, int offset, int length) throws IOException {
        int read = super.read(b, offset, length);
        mLogger.log(b, offset, length);
        return read;
    }

    @Override
    public int available() throws IOException {
        return super.available();
    }

    @Override
    public void close() throws IOException {
        super.close();
    }

    @Override
    public void mark(int readlimit) {
        super.mark(readlimit);
    }

    @Override
    public boolean markSupported() {
        return super.markSupported();
    }


    @Override
    public synchronized void reset() throws IOException {
        super.reset();
    }

    @Override
    public long skip(long n) throws IOException {
        return super.skip(n);
    }

    public interface LoggingInputStreamLogger {
        void log(int read);

        void log(byte[] b, int offset, int length);
    }

    public static class FooLogger implements LoggingInputStreamLogger {
        @Override
        public void log(int read) {
        }

        @Override
        public void log(byte[] b, int offset, int length) {
        }
    }

    public static class AndroidLogger implements LoggingInputStreamLogger {
        private final StringBuilder sb = new StringBuilder();
        private final Map<String, String> map = new HashMap<>();

        @Override
        public void log(int read) {
            sb.append((char) read);
        }

        @Override
        public void log(byte[] b, int offset, int length) {
            sb.append(new String(b, offset, length));
        }

        @Override
        public String toString() {
            return print(null);
        }

        private String print(String s) {
            StringBuilder outSB = new StringBuilder();
            if (map.size() > 0) {
                outSB.append("\nparameters:");
            }
            for (Map.Entry<String, String> entry : map.entrySet()) {
                outSB.append("\n[")
                        .append(entry.getKey())
                        .append(":")
                        .append(entry.getValue())
                        .append("]");
            }
            outSB.append("\nresponse: [").append(s != null ? s : sb.toString()).append("]");

            return outSB.toString();
        }

        /**
         * Divides a string into chunks of a given character size.
         *
         * @param text      String text to be sliced
         * @param sliceSize int Number of characters
         * @return ArrayList<String>   Chunks of strings
         */
        public static ArrayList<String> splitString(String text, int sliceSize) {
            ArrayList<String> textList = new ArrayList<String>();
            String aux;
            int left = -1, right = 0;
            int charsLeft = text.length();
            while (charsLeft != 0) {
                left = right;
                if (charsLeft >= sliceSize) {
                    right += sliceSize;
                    charsLeft -= sliceSize;
                } else {
                    right = text.length();
                    aux = text.substring(left, right);
                    charsLeft = 0;
                }
                aux = text.substring(left, right);
                textList.add("\n" + aux);
            }
            return textList;
        }

        /**
         * Divides a string into chunks.
         *
         * @param text String text to be sliced
         * @return ArrayList<String>
         */
        public static ArrayList<String> splitString(String text) {
            return splitString(text, 3500);
        }

        /**
         * Divides the string into chunks for displaying them
         * into the Eclipse's LogCat.
         *
         * @param text The text to be split and shown in LogCat
         * @param tag  The tag in which it will be shown.
         */
        public static void splitAndLog(String tag, String text) {
            ArrayList<String> messageList = splitString(text);
            if (messageList.size() > 0) {
                int i = 0;
                for (String message : messageList) {
                    Logger.i(tag, "LogResp[" + ++i + "/" + messageList.size() + "]: " + message);
                }
            }
        }
    }
}
