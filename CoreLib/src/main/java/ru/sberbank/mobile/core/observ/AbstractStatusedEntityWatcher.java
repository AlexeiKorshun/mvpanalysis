package ru.sberbank.mobile.core.observ;

import android.content.Context;

import ru.sberbank.mobile.core.alert.actions.InteractionAlertAction;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.bean.operation.StatusedEntity;
import ru.sberbank.mobile.core.check.CheckOptions;
import ru.sberbank.mobile.core.check.IResultChecker;
import ru.sberbank.mobile.core.check.ValidationResult;

/**
 * Created by krygin on 07.11.16.
 */
public abstract class AbstractStatusedEntityWatcher<T extends StatusedEntity> extends SimpleContentWatcher<T> {

    private final IResultChecker<? super T> mResultChecker;

    public AbstractStatusedEntityWatcher(Context context,
                                         IResultChecker<? super T> resultChecker, PendingResult<T> pendingResult) {
        super(context);
        mResultChecker = resultChecker;
        setPendingResult(pendingResult);
    }

    @Override
    protected void onLoaded(final T result) {
        if (result != null) {
            ValidationResult validationResult = mResultChecker.validateResult(result, new CheckOptions(new InteractionAlertAction(new InteractionAlertAction.OnAlertActionEventListener() {
                @Override
                public void onEvent() {
                    onFailure(AbstractStatusedEntityWatcher.this, result, true);
                    onFailureAndAlertsDismissed(AbstractStatusedEntityWatcher.this, result);
                }
            }), true));
            if (validationResult.equals(ValidationResult.VALID)) {
                boolean isMessagesShown = false;
                if (shouldShowSuccessMessages()) {
                    isMessagesShown = mResultChecker.tryShowSuccessMessages(result, new CheckOptions(new InteractionAlertAction(new InteractionAlertAction.OnAlertActionEventListener() {
                        @Override
                        public void onEvent() {
                            onSuccessAndAlertsDismissed(AbstractStatusedEntityWatcher.this, result);
                        }
                    }), true));
                }
                onSuccess(AbstractStatusedEntityWatcher.this, result, isMessagesShown);
            } else if (validationResult.equals(ValidationResult.INVALID_IMMEDIATE)) {
                onFailure(this, result, false);
            }
        }
    }

    protected void onSuccess(IContentWatcher watcher, T result, boolean isAlertsAreShowing) {}
    protected void onFailure(IContentWatcher watcher, T result, boolean isAlertsAreShowing) {}
    protected void onSuccessAndAlertsDismissed(IContentWatcher watcher, T result) {}
    protected void onFailureAndAlertsDismissed(IContentWatcher watcher, T result) {}
    protected boolean shouldShowSuccessMessages() {
        return true;
    }
}
