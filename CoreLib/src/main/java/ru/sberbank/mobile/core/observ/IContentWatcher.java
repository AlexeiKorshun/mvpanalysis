package ru.sberbank.mobile.core.observ;

import android.net.Uri;

/**
 * Created by krygin on 15.11.16.
 */

public interface IContentWatcher extends IWatcher  {
    boolean isConditionalContentWatcher();
    Uri getPendingResultUri();
    void refresh();

    void forceReload();

    void detachResult();
}
