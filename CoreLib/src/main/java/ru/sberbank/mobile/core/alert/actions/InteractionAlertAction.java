package ru.sberbank.mobile.core.alert.actions;

import android.support.annotation.NonNull;

import java.io.Serializable;

import ru.sberbank.mobile.core.alert.AlertAction;
import ru.sberbank.mobile.core.alert.AlertDialogFragment;

/**
 * Created by krygin on 18.10.16.
 */

public class InteractionAlertAction extends AlertAction {

    private transient final OnAlertActionEventListener mListener;

    public InteractionAlertAction(OnAlertActionEventListener listener) {
        mListener = listener;
    }

    @Override
    public void onEvent(@NonNull AlertDialogFragment sender) {
        mListener.onEvent();
    }

    public interface OnAlertActionEventListener {
        void onEvent();
    }
}
