package ru.sberbank.mobile.core.check;

/**
 * @author QuickNick
 */
public enum ValidationResult {

    VALID, // Результат полностью валиден
    INVALID_IMMEDIATE, // Результат невалиден, прерывающих работу сообщений не показано
    INVALID_AND_SHOWING // Результат невалиден, прерывающие работу сообщения показаны
}
