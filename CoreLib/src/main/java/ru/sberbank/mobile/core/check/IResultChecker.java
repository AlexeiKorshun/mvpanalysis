package ru.sberbank.mobile.core.check;

import android.support.annotation.NonNull;

import ru.sberbank.mobile.core.bean.operation.ServerEntity;
import ru.sberbank.mobile.core.bean.operation.StatusedEntity;

/**
 * <p>Чекер результатов операции.</p>
 *
 * <p>Шаги использования.</p>
 * <ol>
 * <li>При получении результатов от менеджера необходимо создать IResultChecker и вызвать его метод validateResult().</li>
 * <li>Если результат успешен, то возможно вызвать tryShowSuccessMessages().</li>
 * </ol>
 *
 * @author QuickNick
 */
public interface IResultChecker<T extends StatusedEntity> {

    CheckOptions DEFAULT_OPTIONS = new CheckOptions();

    /**
     * <p>Валидирует результат выполнения операции и при необходимости показывает ошибки.</p>
     *
     * <p>Важно: присутствующие в EribServerStatusInfo errors/warnings будут показаны лишь в том случае,
     * если EribServerStatusCode != SUCCESS. Для кейса "Показать errors/warnings со статусом SUCCESS"
     * есть метод tryShowSuccessMessages()</p>
     *
     * @param entity  -- результат выполнения операции
     * @param options -- дополнительные опции и данные
     * @return валидность результата
     */
    ValidationResult validateResult(@NonNull T entity, @NonNull CheckOptions options);

    /**
     * Пытается показать сообщения от сервера, если они есть.
     *
     * @param entity  -- результат выполнения операции
     * @param options -- дополнительные опции и данные
     * @return true, если сообщения показаны, в ином случае false
     */
    boolean tryShowSuccessMessages(@NonNull T entity, @NonNull CheckOptions options);
}
