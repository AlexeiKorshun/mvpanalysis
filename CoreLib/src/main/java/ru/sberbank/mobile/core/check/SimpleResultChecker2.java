package ru.sberbank.mobile.core.check;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import java.util.ArrayList;
import java.util.List;

import ru.sberbank.mobile.core.R;
import ru.sberbank.mobile.core.alert.AlertDescription;
import ru.sberbank.mobile.core.alert.AlertDialogFragment;
import ru.sberbank.mobile.core.bean.operation.ServerEntity;
import ru.sberbank.mobile.core.network.ConnectorStatus;

/**
 * Чекер результатов, который используется в сэмплах без {@link ru.sberbank.mobile.core.view.IViewDispatcher}
 *
 * @author QuickNick
 */
public class SimpleResultChecker2 implements IResultChecker<ServerEntity> {

    private final Context mContext;
    private final FragmentManager mFragmentManager;

    public SimpleResultChecker2(Context context, FragmentManager fragmentManager) {
        mContext = context;
        mFragmentManager = fragmentManager;
    }
    @Override
    public ValidationResult validateResult(@NonNull ServerEntity entity, @NonNull CheckOptions options) {
        ValidationResult validationResult = ValidationResult.VALID;
        ConnectorStatus connectorStatus = entity.getConnectorStatus();
        List<String> texts = new ArrayList<>();
        if (connectorStatus != ConnectorStatus.SUCCESS) {
            texts.add(mContext.getResources().getString(connectorStatus.getTextResId()));
            validationResult = ValidationResult.INVALID_AND_SHOWING;
            showDialog(texts, options);
        }
        entity.setHandled(true);
        return validationResult;
    }

    @Override
    public boolean tryShowSuccessMessages(@NonNull ServerEntity entity, @NonNull CheckOptions options) {
        return false;
    }

    private void showDialog(List<String> texts, CheckOptions options) {
        AlertDescription description = new AlertDescription();
        description.setTitle(R.string.warning);
        StringBuilder sb = new StringBuilder();
        for (String text : texts) {
            if (sb.length() > 0) {
                sb.append("\n");
            }
            sb.append(text);
        }
        description.setMessage(sb.toString());
        description.setPositiveButton(new AlertDescription.ButtonAction(R.string.ok, null));
        description.setOnDismissAction(options.afterShowAction);

        AlertDialogFragment fragment = AlertDialogFragment.newInstance(description);
        fragment.show(mFragmentManager, AlertDialogFragment.TAG);
    }
}
