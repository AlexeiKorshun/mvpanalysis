package ru.sberbank.mobile.core.observ;

import android.content.Context;

import ru.sberbank.mobile.core.bean.operation.StatusedEntity;
import ru.sberbank.mobile.core.check.IResultChecker;

/**
 * Created by krygin on 01.12.16.
 */

public abstract class UnconditionalStatusedEntityWatcher<T extends StatusedEntity> extends AbstractStatusedEntityWatcher<T> implements IContentWatcher {

    public UnconditionalStatusedEntityWatcher(Context context, IResultChecker<? super T> resultChecker) {
        super(context, resultChecker, null);
    }

    @Override
    public boolean isConditionalContentWatcher() {
        return false;
    }
}
