package ru.sberbank.mobile.core.bean.text;

import android.os.Parcel;

import junit.framework.TestCase;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import ru.sberbank.mobile.core.CoreEntitiesGenerator;

/**
 * @author QuickNick
 */
public class TextWrapperTest extends TestCase {

    private static final int ATTEMPTS = 5;

    public void testParcelability() {
        for (int i = 0; i < ATTEMPTS; i++) {
            Parcel parcel = Parcel.obtain();
            try {
                TextWrapper expected = CoreEntitiesGenerator.createTextWrapper();
                parcel.writeParcelable(expected, 0);

                parcel.setDataPosition(0);
                TextWrapper actual = parcel.readParcelable(getClass().getClassLoader());
                assertThat(actual, is(expected));
            } finally {
                parcel.recycle();
            }
        }
    }
}
