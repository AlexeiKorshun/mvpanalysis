package ru.sberbank.mobile.core.utils;

import android.net.Uri;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

/**
 * <p></p>
 *
 * @author sokol @ 02.08.16
 */
@RunWith(AndroidJUnit4.class)
public class UriUtilsTest {

    private static final String[] RAW_URIS = new String[] {
            "android-app://ru.sberbankmobile/example",
            "android-app://ru.sberbankmobile/example/",
            "android-app://ru.sberbankmobile/example?foo=bar",
            "android-app://ru.sberbankmobile/example#foo",
            "android-app://ru.sberbankmobile/example/foo/bar/"
    };

    private static final String[] RESULT_URIS = new String[] {
            "android-app://ru.sberbankmobile/example/100500",
            "android-app://ru.sberbankmobile/example/100500",
            "android-app://ru.sberbankmobile/example/100500?foo=bar",
            "android-app://ru.sberbankmobile/example/100500#foo",
            "android-app://ru.sberbankmobile/example/foo/bar/100500"
    };

    private static final String[] ILLEGAL_URIS = new String[] {
            "mailto:test@example.com",
            "/dev/null"
    };

    private static final Uri PARENT = Uri.parse("android-app://ru.sberbankmobile/example/");
    private static final Uri CHILD = Uri.parse("android-app://ru.sberbankmobile/example/100500/abc");
    private static final Uri NOT_A_CHILD = Uri.parse("android-app://ru.sberbankmobile/foo/100500/abc");

    private static final String [] CHILD_PATH = new String [] {"100500", "abc"};

    private static final Uri NORMALIZE_URI =
            Uri.parse("android-app://ru.sberbankmobile/example/");

    private static final Uri [] NOT_NORMALIZE_URI_LIST = new Uri [] {
            Uri.parse("android-app://ru.sberbankmobile/example/"),
            Uri.parse("android-app://ru.sberbankmobile/example/?a=3"),
            Uri.parse("android-app://ru.sberbankmobile/example/?a=3&b=1"),
            Uri.parse("android-app://ru.sberbankmobile/example?a=3"),
            Uri.parse("android-app://ru.sberbankmobile/example?a=3&b=1"),
            Uri.parse("android-app://ru.sberbankmobile/example")
    };

    private static final Uri LONG_URI = Uri.parse("android-app://ru.sberbankmobile/example/");

    public static final long TEST_ID = 100500;

    @Test
    public void testWithAppendedId() throws Exception {

        // Проверяем сам тест
        assertEquals("Error in test case", RAW_URIS.length, RESULT_URIS.length);

        for (int i = 0; i < RAW_URIS.length; i++) {
            Uri uri = Uri.parse(RAW_URIS[i]);
            Uri result = UriUtils.withAppendedId(uri, TEST_ID);
            assertTrue(result.toString().startsWith(RESULT_URIS[i]));
        }

        for (String illegal: ILLEGAL_URIS) {
            Uri uri = Uri.parse(illegal);

            try {
                UriUtils.withAppendedId(uri, TEST_ID);
            } catch (IllegalArgumentException okay) {
                continue;
            }

            // Если не было эксепшена, то тест свалится
            assertTrue("Illegal uri passed: " + illegal, false);
        }
    }

    @Test
    public void testEscapePath() throws Exception {
        assertEquals("card/100500", UriUtils.escapePath("card:100500"));
        assertEquals("card/100500", UriUtils.escapePath(" card:100500 "));
    }

    @Test
    public void testIsNestedUri() {

        Uri[] goodParents = new Uri[] {
                Uri.parse("android-app://ru.sberbankmobile/foo"),
                Uri.parse("android-app://ru.sberbankmobile/foo/"),
                Uri.parse("android-app://ru.sberbankmobile/foo?cat=1")
        };

        Uri[] badParents = new Uri[] {
                Uri.parse("android-app://ru.sberbankmobile/bar"),
                Uri.parse("android-app://ru.sberbankmobile/bar/?cat=1")
        };

        Uri[] goodChilds = new Uri[] {
                Uri.parse("android-app://ru.sberbankmobile/foo/bar"),
                Uri.parse("android-app://ru.sberbankmobile/foo/foo"),
                Uri.parse("android-app://ru.sberbankmobile/foo/bar/"),
                Uri.parse("android-app://ru.sberbankmobile/foo/foo?cat=1&dog=2"),
                Uri.parse("android-app://ru.sberbankmobile/foo/bar?dog=2"),
                Uri.parse("android-app://ru.sberbankmobile/foo/b"),
                Uri.parse("android-app://ru.sberbankmobile/foo/bar/and/very/long/path")
        };

        Uri[] badChilds = new Uri[] {
                Uri.parse("android-app://ru.sberbankmobile/foo/"),
                Uri.parse("android-app://ru.sberbankmobile/foo?cat=1"),
                Uri.parse("android-app://ru.sberbankmobile/foobar"),
                Uri.parse("android-app://ru.sberbankmobile/something/else")
        };

        for (int i = 0; i < goodParents.length; i++) {
            for (int k = 0; k < goodChilds.length; k++) {
                assertTrue(UriUtils.isNestedUri(goodParents[i], goodChilds[k]));
                assertFalse(UriUtils.isNestedUri(goodChilds[k], goodParents[i]));
            }

            for (int k = 0; k < badChilds.length; k++) {
                assertFalse(UriUtils.isNestedUri(goodParents[i], badChilds[i]));
            }
        }

        for (int i = 0; i < badParents.length; i++) {
            for (int k = 0; k < goodChilds.length; k++) {
                assertFalse(UriUtils.isNestedUri(badParents[i], goodChilds[k]));
            }

            for (int k = 0; k < badChilds.length; k++) {
                assertFalse(UriUtils.isNestedUri(badParents[i], badChilds[k]));
            }
        }
    }

    @Test
    public void testIsEqualOrNestedUri() {
        assertTrue(UriUtils.isEqualOrNestedUri(PARENT, CHILD));
        assertTrue(UriUtils.isEqualOrNestedUri(PARENT, PARENT));
        assertTrue(UriUtils.isEqualOrNestedUri(CHILD, CHILD));

        assertFalse(UriUtils.isEqualOrNestedUri(CHILD, PARENT));
        assertFalse(UriUtils.isEqualOrNestedUri(NOT_A_CHILD, PARENT));
        assertFalse(UriUtils.isEqualOrNestedUri(PARENT, NOT_A_CHILD));
    }

    @Test
    public void testIsEqualOrNestedString() {
        assertFalse(UriUtils.isEqualOrNestedString("abc", "ab"));
        assertFalse(UriUtils.isEqualOrNestedString("abc", "abf"));
        assertTrue(UriUtils.isEqualOrNestedString("abc", "abc"));
        assertTrue(UriUtils.isEqualOrNestedString("abc", "abcd"));
    }

    @Test
    public void testGetRestPathSegments() throws Exception {
        assertEquals(UriUtils.getRestPathSegments(PARENT, CHILD), Arrays.asList(CHILD_PATH));
        assertEquals(UriUtils.getRestPathSegments(PARENT, NOT_A_CHILD), new ArrayList<String>());

    }

    @Test
    public void testCutQueryParamsAndNormalize() throws Exception {
        for(Uri raw : NOT_NORMALIZE_URI_LIST) {
            assertEquals(UriUtils.cutQueryParamsAndNormalize(raw.toString()), NORMALIZE_URI.toString());
        }
    }

}