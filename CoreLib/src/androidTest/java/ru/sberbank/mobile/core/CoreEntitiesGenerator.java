package ru.sberbank.mobile.core;

import android.support.annotation.NonNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.common.rate.entity.Rate;
import ru.sberbank.mobile.common.rate.entity.RatesBundle;
import ru.sberbank.mobile.core.alert.AlertAction;
import ru.sberbank.mobile.core.alert.AlertDescription;
import ru.sberbank.mobile.core.alert.AlertDialogFragment;
import ru.sberbank.mobile.core.bean.money.Currency;
import ru.sberbank.mobile.core.bean.text.TextWrapper;

/**
 * @author QuickNick
 */
public class CoreEntitiesGenerator extends BaseEntitiesGenerator {

    public static TextWrapper createTextWrapper() {
        TextWrapper wrapper = null;
        if (RANDOM.nextBoolean()) {
            wrapper = new TextWrapper(createRandomIntId());
        } else {
            wrapper = new TextWrapper(createString());
        }
        return wrapper;
    }

    public static AlertDescription createAlertDescription() {
        AlertDescription description = new AlertDescription();
        description.setTitle(createTextWrapper());
        description.setMessage(createTextWrapper());
        description.setPositiveButton(createAlertDescriptionAction());
        description.setNeutralButton(createAlertDescriptionAction());
        description.setNegativeButton(createAlertDescriptionAction());
        description.setOnDismissAction(new ActionImpl());
        return description;
    }

    public static Rate createRate() {
        Rate rate = new Rate()
                .setCurrency(createEnumValue(Currency.values()))
                .setRelation(new BigDecimal(RANDOM.nextDouble()))
                .setScale(new BigDecimal(RANDOM.nextInt()));
        return rate;
    }

    public static RatesBundle createRatesBundle() {
        RatesBundle bundle = new RatesBundle();
        int size = RANDOM.nextInt(MAX_TEST_LIST_SIZE) + 1;
        List<Rate> rates = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            rates.add(createRate());
        }
        bundle.setRates(rates);
        return bundle;
    }

    public static Person createPerson(boolean includeId) {
        Person person = new Person();
        if (includeId) {
            person.id = Math.abs(RANDOM.nextInt());
        }
        person.name = createString();
        return person;
    }

    public static List<Person> createPersonsList(boolean includeId) {
        int size = RANDOM.nextInt(MAX_TEST_LIST_SIZE) + 1;
        List<Person> persons = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            persons.add(createPerson(includeId));
        }
        return persons;
    }

    public static void updatePerson(Person person) {
        person.name = createString();
    }

    private static AlertDescription.ButtonAction createAlertDescriptionAction() {
        AlertDescription.ButtonAction action = new AlertDescription.ButtonAction(createTextWrapper(),
                new ActionImpl());
        return action;
    }

    private static class ActionImpl extends AlertAction {

        @Override
        public void onEvent(@NonNull AlertDialogFragment sender) {
        }

        @Override
        public boolean equals(Object o) {
            return true;
        }

        @Override
        public int hashCode() {
            return 0;
        }
    }
}
