package ru.sberbank.mobile.common.person;

import android.net.Uri;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.CoreEntitiesGenerator;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author QuickNick
 */
@RunWith(AndroidJUnit4.class)
public class PersonUrisTest {

    @Test
    public void testIsAddPersonUri() {
        Uri addPersonUri = PersonUris.getAddPersonUri(CoreEntitiesGenerator.createPerson(false));
        Uri updatePersonUri = PersonUris.getUpdatePersonUri(CoreEntitiesGenerator.createPerson(true));

        assertThat(PersonUris.isAddPersonUri(addPersonUri), is(true));
        assertThat(PersonUris.isAddPersonUri(updatePersonUri), is(false));
    }

    @Test
    public void testIsUpdatePersonUri() {
        Uri addPersonUri = PersonUris.getAddPersonUri(CoreEntitiesGenerator.createPerson(false));
        Uri updatePersonUri = PersonUris.getUpdatePersonUri(CoreEntitiesGenerator.createPerson(true));

        assertThat(PersonUris.isUpdatePersonUri(addPersonUri), is(false));
        assertThat(PersonUris.isUpdatePersonUri(updatePersonUri), is(true));
    }
}
