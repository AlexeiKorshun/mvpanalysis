package ru.sberbank.mobile.common.rate.net;

import ru.sberbank.mobile.common.rate.entity.RatesBundle;

/**
 * @author QuickNick.
 */
public class StubCurrencyRateApiMapper implements ICurrencyRateApiMapper {

    private RatesBundle mRatesBundle;

    @Override
    public RatesBundle getRatesBundle() {
        return mRatesBundle;
    }

    public void setRatesBundle(RatesBundle ratesBundle) {
        mRatesBundle = ratesBundle;
    }
}
