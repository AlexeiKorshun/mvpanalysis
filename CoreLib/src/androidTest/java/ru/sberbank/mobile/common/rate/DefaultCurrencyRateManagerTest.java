package ru.sberbank.mobile.common.rate;

import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.sberbank.mobile.common.rate.entity.RatesBundle;
import ru.sberbank.mobile.common.rate.net.StubCurrencyRateApiMapper;
import ru.sberbank.mobile.core.CoreEntitiesGenerator;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.async.StubAsyncProcessor;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * @author QuickNick.
 */
@RunWith(AndroidJUnit4.class)
public class DefaultCurrencyRateManagerTest {

    private StubAsyncProcessor mAsyncProcessor;
    private StubCurrencyRateApiMapper mApiMapper;
    private DefaultCurrencyRateManager mManager;

    @Before
    public void setUp() {
        mAsyncProcessor = new StubAsyncProcessor(InstrumentationRegistry.getContext());
        mApiMapper = new StubCurrencyRateApiMapper();
        mManager = new DefaultCurrencyRateManager(mAsyncProcessor, mApiMapper);
    }

    @Test
    public void testGetRatesBundle() {
        RatesBundle expected = CoreEntitiesGenerator.createRatesBundle();
        mApiMapper.setRatesBundle(expected);

        PendingResult<RatesBundle> result = mManager.getRatesBundle();
        assertThat(result.awaitAndGetResult(), is(expected));
        assertThat(result.getUri(), is(RateUris.RATES_BUNDLE_URI));
    }
}
