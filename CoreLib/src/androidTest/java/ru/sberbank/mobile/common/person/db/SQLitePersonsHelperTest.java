package ru.sberbank.mobile.common.person.db;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.sberbank.mobile.common.person.db.PersonsContract;
import ru.sberbank.mobile.common.person.db.SQLitePersonsHelper;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

/**
 * @author QuickNick.
 */
@RunWith(AndroidJUnit4.class)
public class SQLitePersonsHelperTest {

    private static final String DB_NAME = "test_persons.db";
    private SQLitePersonsHelper mOpenHelper;

    @Test
    public void testCreation() {
        mOpenHelper = new SQLitePersonsHelper(InstrumentationRegistry.getContext(), DB_NAME);
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor cursor = null;
        try {
            cursor = db.query(SQLitePersonsHelper.TABLE_PERSONS, null, null, null, null, null, null);
            assertThat(cursor, notNullValue());
            for (String column : PersonsContract.Persons.ALL_COLUMNS) {
                int columnIndex = cursor.getColumnIndex(column);
                assertThat(columnIndex, not(-1));
            }
            assertThat(cursor.getColumnCount(), is(PersonsContract.Persons.ALL_COLUMNS.length));
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }
    }

    @After
    public void tearDown() {
        InstrumentationRegistry.getContext().deleteDatabase(DB_NAME);
    }
}
