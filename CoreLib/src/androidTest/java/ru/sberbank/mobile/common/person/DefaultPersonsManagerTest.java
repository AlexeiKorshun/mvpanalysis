package ru.sberbank.mobile.common.person;

import android.net.Uri;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import ru.sberbank.mobile.common.person.db.StubPersonsDao;
import ru.sberbank.mobile.common.person.entity.AddPersonResponse;
import ru.sberbank.mobile.common.person.entity.Person;
import ru.sberbank.mobile.core.CoreEntitiesGenerator;
import ru.sberbank.mobile.core.async.PendingResult;
import ru.sberbank.mobile.core.async.StubAsyncProcessor;

/**
 * @author QuickNick.
 */
@RunWith(AndroidJUnit4.class)
public class DefaultPersonsManagerTest {

    private StubAsyncProcessor mAsyncProcessor;
    private StubPersonsDao mPersonsDao;
    private DefaultPersonsManager mManager;

    @Before
    public void setUp() {
        mAsyncProcessor = new StubAsyncProcessor(InstrumentationRegistry.getContext());
        mPersonsDao = new StubPersonsDao();
        mManager = new DefaultPersonsManager(mAsyncProcessor, mPersonsDao);
    }

    @Test
    public void testGetPersons() {
        List<Person> expected = CoreEntitiesGenerator.createPersonsList(false);
        mPersonsDao.addPersonsForTest(expected);

        PendingResult<List<Person>> result = mManager.getPersons();
        assertThat(result.awaitAndGetResult(), is(expected));
        assertThat(result.getUri(), is(PersonUris.PERSONS_URI));
    }

    @Test
    public void testAddPerson() {
        Person person = CoreEntitiesGenerator.createPerson(false);
        Uri expectedUri = PersonUris.getAddPersonUri(person);

        PendingResult<AddPersonResponse> result = mManager.addPerson(person);
        assertThat(result.awaitAndGetResult().id, is(person.id));
        assertThat(result.getUri(), is(expectedUri));
        assertThat(mPersonsDao.getPersons().contains(person), is(true));
    }

    @Test
    public void testUpdatePerson() {
        Person person = CoreEntitiesGenerator.createPerson(false);
        mPersonsDao.addPersonForTest(person);
        Uri expectedUri = PersonUris.getUpdatePersonUri(person);

        CoreEntitiesGenerator.updatePerson(person);
        PendingResult<Integer> result = mManager.updatePerson(person);
        assertThat(result.awaitAndGetResult(), is(1));
        assertThat(result.getUri(), is(expectedUri));
        assertThat(mPersonsDao.getPersons().contains(person), is(true));
    }

    @Test
    public void testDeletePerson() {
        Person person = CoreEntitiesGenerator.createPerson(false);
        mPersonsDao.addPersonForTest(person);
        Uri expectedUri = PersonUris.getDeletePersonUri(person);

        PendingResult<Integer> result = mManager.deletePerson(person);
        assertThat(result.awaitAndGetResult(), is(1));
        assertThat(result.getUri(), is(expectedUri));
        assertThat(mPersonsDao.getPersons().contains(person), is(false));
    }
}
